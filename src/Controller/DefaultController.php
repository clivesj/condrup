<?php

namespace Drupal\condrup\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Default controller for the Conscribo module.
 */
class DefaultController extends ControllerBase {

  /**
   * @function callback for condrup/sync/user
   * Syncs exactly one user from Conscribo to Drupal
   * @param integer $uid UID of user to be updated
   * @return array
   */
  public function syncConscriboToUser($uid) {
    /**
     * @var \Drupal\condrup\Services\Common $common
     * @var User $user
     */
    $common = \Drupal::service('condrup.common');
    $user = User::load($uid);
    if ($cid = $user->field_conscribo_id->value) {
      if ($data = $common->getFromConscribo($cid)) {
        // set the email separately
        $user->setEmail($data['email']['value']);
        unset($data['email']);
        foreach($data as $field) {
          $user->set($field['label'], $field['value']);
        }
        $user->save();
        \Drupal::messenger()->addMessage('User updated');
      }
      else {
        \Drupal::messenger()->addWarning('Conscribo data not found');
      }
    }
    else {
      \Drupal::messenger()->addWarning('CID not found');
    }
    return new RedirectResponse('/user/' . $uid );
  }

// deprecated
//  /**
//   * @file updates all active users with Conscribo data
//   * callback path condrup/sync
//   */
//  public function syncConscriboToDrupal() {
//    // get all the uids to be updated:
//    // fixme: remove range
//    $ids = \Drupal::entityQuery('user')
//      // we only update active users
//      ->condition('status', 1)
//      //->condition('field_conscribo_id', ">", 0)
//      ->range(100, 20)
//      ->execute();
//    $users = User::loadMultiple($ids);
//
//    $batch = array(
//        'title' => t('Sync from Conscribo to Drupal ...'),
//        'operations' => [],
//        'init_message' => $this->t('Start sync'),
//        'progress_message' => $this->t('Processed @current out of @total.'),
//        'error_message' => $this->t('An error occurred during processing'),
//        'finished' => '\Drupal\condrup\ConscriboSync::finishedSync',
//    );
//
//    foreach ($users as $user) {
//      $batch['operations'][] = ['Drupal\condrup\ConscriboSync::syncUserFromConscribo', [$user]];
//    }
//    batch_set($batch);
//    return batch_process('admin/people');
//  }

//  public function syncFromConscribo() {
//    /**
//     * @var \Drupal\condrup\Services\Common $common
//     * @var User $user
//     */
//    $common = \Drupal::service('condrup.common');
//
//    // get all the uid's to be updated:
//    $ids = \Drupal::entityQuery('user')
//        ->condition('status', 1)
//        ->execute();
//    $users = User::loadMultiple($ids);
//
//    $batch = array(
//        'title' => t('Sync to Conscribor...'),
//        'operations' => [],
//        'init_message' => $this->t('Start sync'),
//        'progress_message' => $this->t('Processed @current out of @total.'),
//        'error_message' => $this->t('An error occurred during processing'),
//        'finished' => '\Drupal\condrup\ConscriboSync::finishedSync',
//    );
//
//    foreach ($users as $user) {
//      $batch['operations'][] = ['Drupal\condrup\ConscriboSync::syncUser', [$user]
//      ];
//    }
//    batch_set($batch);
//    return batch_process('admin/people');
//
//  }



}
