<?php

namespace Drupal\condrup;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Designer entity entity.
 *
 * @see \Drupal\condrup\Entity\DesignerEntity.
 */
class DesignerEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\condrup\Entity\DesignerEntityInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished designer entity entities');
        }


        return AccessResult::allowedIfHasPermission($account, 'view published designer entity entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit designer entity entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete designer entity entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add designer entity entities');
  }


}
