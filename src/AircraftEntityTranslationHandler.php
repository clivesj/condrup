<?php

namespace Drupal\condrup;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for aircraft_entity.
 */
class AircraftEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
