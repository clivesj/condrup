<?php
/**
 * User: Dre
 * Date: 8-12-2016
 * Time: 11:38
 */

namespace Drupal\condrup\ConscriboConnector\Response;

use Drupal\condrup\ConscriboConnector\Response;

class ListEntityTypesResponse extends Response {

	/**
	 * @var array
	 *    array(array('typeName' => string(7) "persoon"
	 *                'langDeterminer' => string(4) "deze"
	 *                'langSingular' => string(7) "persoon"
	 *                'langPlural' => string(8) "personen"))
	 */
	private $entityTypes;

	protected function setResponseFromArray($responseArray) {
		parent::setResponseFromArray($responseArray);

		if(!$this->getIsSuccessFull()) {
			return;
		}
		foreach($responseArray['entityTypes'] as $fieldStruct) {
			$this->entityTypes[$fieldStruct['typeName']] = $fieldStruct;
		}
	}

	/**
	 * @return array
	 */
	public function getEntityTypeNames() {
		$res = array();
		foreach($this->entityTypes as $typeName => $entityType) {
			$res[$typeName] = $typeName;
		}
		return $res;
	}

	/**
	 * @param $typeName
	 * @param bool $plural
	 * @return string
	 */
	public function getTypeNameFormatted($typeName, $plural = false) {
		return $this->entityTypes[$typeName]['langDeterminer'] . (($plural) ? $this->entityTypes['langPlural'] : $this->entityTypes['langSingular']);
	}

	public function count() {
		return count($this->entityTypes);
	}

}