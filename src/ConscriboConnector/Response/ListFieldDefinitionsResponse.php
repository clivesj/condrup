<?php
/**
 * User: Dre
 * Date: 8-12-2016
 * Time: 11:38
 */

namespace Drupal\condrup\ConscriboConnector\Response;

use Drupal\condrup\ConscriboConnector\Response;

class ListFieldDefinitionsResponse extends Response {

	/**
	 * @var array
	 *  array('fieldName' => ...,
	 * 			'entityType => ...,
	 * 			'label' => ...,
	 * 			'description' => ..,
	 * 			'type' => ...,
	 * 			'required' => ...,
	 * 			'readOnly' => ...,)
	 */
	private $fields;

	protected function setResponseFromArray($responseArray) {
		parent::setResponseFromArray($responseArray);

		if(!$this->getIsSuccessFull()) {
			return;
		}

		foreach($responseArray['fields'] as $fieldStruct) {
			$this->fields[$fieldStruct['fieldName']] = $fieldStruct;
		}
	}

	/**
	 * @return array
	 */
	public function getFields() {
		return $this->fields;
	}
	/**
	 * @return string[]
	 */
	public function getFieldNames() {
		return array_keys($this->fields);
	}

	/**
	 * @param $fieldName
	 * @return mixed|null
	 */
	public function getFieldWithName($fieldName) {
		if(isset($this->fields[$fieldName])) {
			return $this->fields[$fieldName];
		}
		return NULL;
	}

	public function count() {
		return count($this->fields);
	}

	public function getFieldType($fieldName) {
		$field = $this->getFieldWithName($fieldName);
		if($field === NULL) {
			return NULL;
		}
		return $field['type'];
	}

	public function getFieldIsReadOnly($fieldName) {
		$field = $this->getFieldWithName($fieldName);
		if($field === NULL) {
			return NULL;
		}
		return $field['readOnly'];
	}
}