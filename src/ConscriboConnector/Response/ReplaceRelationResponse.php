<?php
/**
 * User: Dre
 * Date: 8-12-2016
 * Time: 11:38
 */

namespace Drupal\condrup\ConscriboConnector\Response;

use Drupal\condrup\ConscriboConnector\Response;

class ReplaceRelationResponse extends Response {

	/**
	 * @var String
	 */
	private $relationNr;

	public function __construct() {
		parent::__construct();
	}


	protected function setResponseFromArray($responseArray) {
		parent::setResponseFromArray($responseArray);

		if(!$this->getIsSuccessFull()) {
			return;
		}
		$this->relationNr = $responseArray['code'];
	}

	/**
	 * Return the assigned relationNr (code) of the updated/added relation
	 * @return String
	 */
	public function getRelationNr() {
		return $this->relationNr;
	}
}