<?php
/**
 * User: Dre
 * Date: 8-12-2016
 * Time: 11:38
 */

namespace Drupal\condrup\ConscriboConnector\Response;

use Drupal\condrup\ConscriboConnector\Response;

class ListVatCodesResponse extends Response {
	/**
	 * @var array array('code' => 'H',
	'name' => '21% (H)',
	'percentage' => '21,00',
	'isReverseChargeGroup' => 0,
	'sectionToPay' => '1a',
	'sectionToReceive' => '5b',
	'sectionToPayReverseCharge' => NULL,)
	 */
	private $vatCodes;

	public function __construct() {
		parent::__construct();
		$this->vatCodes = array();
	}


	protected function setResponseFromArray($responseArray) {
		parent::setResponseFromArray($responseArray);

		if(!$this->getIsSuccessFull()) {
			return;
		}

		foreach($responseArray['vatCodes'] as $account) {
			$this->vatCodes[$account['code']] = $account;
		}
	}

	/**
	 * @return array  array('code' => 'H',
						'name' => '21% (H)',
						'percentage' => '21,00',
						'isReverseChargeGroup' => 0,
						'sectionToPay' => '1a',
						'sectionToReceive' => '5b',
						'sectionToPayReverseCharge' => NULL,)
	 */
	public function getVatCodes() {
		return $this->vatCodes;
	}

	/**
	 * @return int nr received vatCodes
	 */
	public function count() {
		return count($this->vatCodes);
	}



}