<?php
/**
 * User: Dre
 * Date: 8-12-2016
 * Time: 11:38
 */

namespace Drupal\condrup\ConscriboConnector\Response;

use Drupal\condrup\ConscriboConnector\Response;

class ListAccountsResponse extends Response {
	/**
	 * @var array  array(accountNr => ..
	 * 						accountName => ...
	 * 						type => <result/balance>
	 * 						usedForDebit => 0/1
	 * 						usedForCredit => 0/1
	 * 						transactional => 0/1);
	 */
	private $accounts;

	public function __construct() {
		parent::__construct();
		$this->accounts = array();
	}


	protected function setResponseFromArray($responseArray) {
		parent::setResponseFromArray($responseArray);

		if(!$this->getIsSuccessFull()) {
			return;
		}

		foreach($responseArray['accounts'] as $account) {
			$this->accounts[$account['accountNr']] = $account;
		}
	}

	/**
	 * @return array  array(accountNr => ..
	 * 						accountName => ...
	 * 						type => <result/balance>
	 * 						usedForDebit => 0/1
	 * 						usedForCredit => 0/1
	 * 						transactional => 0/1
	 */
	public function getAccounts() {
		return $this->accounts;
	}

	/**
	 * @return int nr received accounts
	 */
	public function count() {
		return count($this->accounts);
	}

	/**
	 * @param $nr
	 * @return null
	 */
	public function getAccountByNr($nr) {
		if(isset($this->accounts[$nr])) {
			return $this->accounts[$nr];
		}
		return NULL;
	}



}