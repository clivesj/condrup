<?php
/**
 * User: Dre
 * Date: 8-12-2016
 * Time: 11:38
 */

namespace Drupal\condrup\ConscriboConnector\Response;

use Drupal\condrup\ConscriboConnector\Response;

class ListChangedEntityIdsResponse extends Response {

	/**
	 * @var array
	 */
	private $relations;

	public function __construct() {
		parent::__construct();
		$this->relations = array();
	}


	protected function setResponseFromArray($responseArray) {
		parent::setResponseFromArray($responseArray);

		if(!$this->getIsSuccessFull()) {
			return;
		}

		foreach($responseArray['entityIds'] as $entityId) {
			$this->relations[$entityId] = $entityId;
		}
	}

	/**
	 * @return array
	 */
	public function getRelations() {
		return $this->relations;
	}

}