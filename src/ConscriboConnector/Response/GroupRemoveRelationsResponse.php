<?php
/**
 * User: Dre
 * Date: 11-12-2016
 * Time: 14:36
 */

namespace Drupal\condrup\ConscriboConnector\Response;

use Drupal\condrup\ConscriboConnector\Response;

class GroupRemoveRelationsResponse extends Response {

	/**
	 * @var int
	 */
	private $numSucces;

	/**
	 * @var array
	 */
	private $failed;

	public function __construct() {
		parent::__construct();
	}

	protected function setResponseFromArray($responseArray) {
		parent::setResponseFromArray($responseArray);

		if(!$this->getIsSuccessFull()) {
			return;
		}
		$this->numSucces = $responseArray['numSucces'];
		$this->failed = $responseArray['failed'];
	}

	/**
	 * Returns the number of succesful relations removed from a group
	 * @return int
	 */
	public function getNumSucces() {
		return $this->numSucces;
	}

	/**
	 * Returns the list of relations which could not be removed and why
	 * @return array
	 */
	public function getFailed() {
		return $this->failed;
	}

}