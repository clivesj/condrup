<?php
/**
 * User: Dre
 * Date: 11-12-2016
 * Time: 14:36
 */

namespace ConscriboConnector\Response;

use ConscriboConnector\Response;

class ArchiveRelationsResponse extends Response {

	/**
	 * @var int
	 */
	private $numSucces;

	/**
	 * @var array
	 */
	private $failed;

	/**
	 * @var int
	 */
	private $numClearedInformation;


	public function __construct() {
		parent::__construct();
	}

	protected function setResponseFromArray($responseArray) {
		parent::setResponseFromArray($responseArray);

		if(!$this->getIsSuccessFull()) {
			return;
		}
		$this->numSucces = $responseArray['numSucces'];
		$this->failed = $responseArray['failed'];
		$this->numClearedInformation = $responseArray['numClearedInformation'];
	}

	/**
	 * Returns the number of succesful placement in the archive relationgroup
	 * @return int
	 */
	public function getNumSucces() {
		return $this->numSucces;
	}

	/**
	 * Returns the list of relations which could not be archived and why
	 * @return array
	 */
	public function getFailed() {
		return $this->failed;
	}

	/**
	 * Returns the number of relation which information has been removed.
	 * @return int
	 */
	public function getNumClearedInformation() {
		return $this->numClearedInformation;
	}


}