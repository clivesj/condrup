<?php
/**
 * User: Dre
 * Date: 8-12-2016
 * Time: 11:38
 */

namespace Drupal\condrup\ConscriboConnector\Response;

use Drupal\condrup\ConscriboConnector\Response;

class AuthenticateWithUserAndPassResponse extends Response {

	/**
	 * @var String sessionId received from Conscribo
	 */
	public $sessionId;

	public function getSessionId() {
		return $this->sessionId;
	}

	protected function setResponseFromArray($responseArray) {
		parent::setResponseFromArray($responseArray);

		if($this->getIsSuccessFull()) {
			$this->sessionId = $this->responseInfo['sessionId'];
		}
	}


}