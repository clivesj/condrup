<?php
/**
 * User: Dre
 * Date: 11-12-2016
 * Time: 14:36
 */

namespace Drupal\condrup\ConscriboConnector\Response;



use Drupal\condrup\ConscriboConnector\Response;

class AddChangeTransactionResponse extends Response{

	/**
	 * @var int
	 */
	private $transactionId;

	public function __construct() {
		parent::__construct();
	}

	protected function setResponseFromArray($responseArray) {
		parent::setResponseFromArray($responseArray);

		if(!$this->getIsSuccessFull()) {
			return;
		}
		$this->transactionId = $responseArray['transactionId'];
	}

	/**
	 * Returns the assigned transactionId. This is an integer.
	 * @return int
	 */
	public function getTransactionId() {
		return $this->transactionId;
	}


}