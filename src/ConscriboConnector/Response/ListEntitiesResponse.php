<?php
/**
 * User: Dre
 * Date: 8-12-2016
 * Time: 11:38
 */

namespace Drupal\condrup\ConscriboConnector\Response;

use Drupal\condrup\ConscriboConnector\Response;

class ListEntitiesResponse extends Response {

	/**
	 * @var array
	 */
	private $relations;

	/**
	 * @var int Total nr of results disregarding limits and offsets
	 */
	private $resultCount;

	public function __construct() {
		parent::__construct();
		$this->relations = array();
	}


	protected function setResponseFromArray($responseArray) {
		parent::setResponseFromArray($responseArray);

		if(!$this->getIsSuccessFull()) {
			return;
		}

		foreach($responseArray['entities'] as $relation) {
			$this->relations[$relation['code']] = $relation;
		}
		$this->resultCount = $responseArray['resultCount'];
	}

	/**
	 * @return array
	 */
	public function getRelations() {
		return $this->relations;
	}

	/**
	 * @param $relationNr
	 * @return mixed|null
	 */
	public function getRelationWithNr($relationNr) {
		if(isset($this->relations[$relationNr])) {
			return $this->relations[$relationNr];
		}
		return NULL;
	}

	/**
	 * @return int nr received relations
	 */
	public function count() {
		return count($this->relations);
	}

	/**
	 * @return int Total nr of results disregarding limits and offsets
	 */
	public function getTotalResultCount() {
		return $this->resultCount;

	}

}