<?php
/**
 * User: Dre
 * Date: 8-12-2016
 * Time: 11:38
 */

namespace Drupal\condrup\ConscriboConnector\Response;

use Drupal\condrup\ConscriboConnector\Response;

class ListTransactionsResponse extends Response {
	/**
	 * @var array  array (
				'transactionId' => 16,
				'transactionNr' => '16-013',
				'description' => 'One transaction',
				'date' => '2016-12-11',
				'transactionRows' =>
				array (49 =>
					array (
					'accountNr' => '8000',
					'amount' => '50,00',
					'side' => 'debet',
					'reference' => 'F-1',
					'relationNr' => NULL,
					'vatCode' => 'H',	// Only with VAT administration enabled Hoog tarief
					'vatAmount' => 0,00	// Only with VAT administration enabled
					),..
				)
	  		);
	 */
	private $transactions;

	/**
	 * @var int Total nr of results disregarding limits and offsets
	 */
	private $resultCount;

	public function __construct() {
		parent::__construct();
		$this->transactions = array();
	}


	protected function setResponseFromArray($responseArray) {
		parent::setResponseFromArray($responseArray);

		if(!$this->getIsSuccessFull()) {
			return;
		}

		foreach($responseArray['transactions'] as $relation) {
			$this->transactions[$relation['transactionId']] = $relation;
		}
		$this->resultCount = $responseArray['nrTransactions'];
	}

	/**
	 * @return array
	 */
	public function getTransactions() {
		return $this->transactions;
	}

	/**
	 * @return int nr received transactions
	 */
	public function count() {
		return count($this->transactions);
	}

	/**
	 * @return int Total nr of results disregarding limits and offsets
	 */
	public function getTotalResultCount() {
		return $this->resultCount;
	}



}