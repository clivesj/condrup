<?php
/**
 * Created by PhpStorm.
 * User: joop
 * Date: 12/20/20
 * Time: 2:25 PM
 */

namespace Drupal\condrup\ConscriboConnector\Response;

use Drupal\condrup\ConscriboConnector\Response;

class ListEntityGroupsResponse extends Response {

  /**
   * @var array
   */
  private $relations;
  private $groups;

  /**
   * @var int Total nr of results disregarding limits and offsets
   */
  private $resultCount;

  public function __construct() {
    parent::__construct();
    $this->relations = [];
    $this->groups = [];
  }


  protected function setResponseFromArray($responseArray) {
    parent::setResponseFromArray($responseArray);

    if(!$this->getIsSuccessFull()) {
      echo('failed');
      return;
    }
    foreach($responseArray['entityGroups'] as $group ) {
      $this->groups[$group['id']] = $group;
      $cids = [];
      foreach ($group['members'] as $member) {
        array_push($cids, $member['entityId']);
      }
      $this->groups[$group['id']]['cids'] = $cids;
    }
    //$this->resultCount = $responseArray['resultCount'];
  }

  /**
   * @return array
   */
  public function getGroups() {
    return $this->groups;
  }

  /**
   * @return array
   */
  public function getRelations() {
    return $this->relations;
  }

  /**
   * @param $relationNr
   * @return mixed|null
   */
  public function getRelationWithNr($relationNr) {
    if(isset($this->relations[$relationNr])) {
      return $this->relations[$relationNr];
    }
    return NULL;
  }

  /**
   * @return int nr received relations
   */
  public function count() {
    return count($this->relations);
  }

  /**
   * @return int Total nr of results disregarding limits and offsets
   */
  public function getTotalResultCount() {
    return $this->resultCount;

  }

}