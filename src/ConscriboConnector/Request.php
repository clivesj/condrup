<?php
/**
 * User: Dre
 * Date: 7-12-2016
 * Time: 14:54
 */

namespace Drupal\condrup\ConscriboConnector;

abstract class Request {

	/**
	 * @var Response
	 */
	protected $response;

	protected $requestSequence;

	abstract protected function getJSONRequest(ConscriboConnector $connector);

	/**
	 * @return Request
	 */
	static function create() {
		$className = get_called_class();
		$obj = new $className();
		return $obj;
	}

	function __construct() {
		$this->requestSequence = NULL;
	}

	public function setRequestSequence($sequence) {
		$this->requestSequence = $sequence;
	}

	public function getRequestSequence() {
		return $this->requestSequence;
	}
	/**
	 * @param Response $response
	 */
	public function setResponse(Response $response) {
		$this->response = $response;
	}

	public function getJSON($connector) {
		$res = $this->getJSONRequest($connector);
		if(isset($this->requestSequence) && !isset($res['requestSequence'])) {
			$res['requestSequence'] = $this->requestSequence;
		}
		return $res;
	}

	/**
	 * @return Response
	 */
	public function getResponse() {
		return $this->response;
	}





}