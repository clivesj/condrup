<?php
/**
 * User: Dre
 * Date: 7-12-2016
 * Time: 17:03
 */

namespace Drupal\condrup\ConscriboConnector;


class Response {

	/**
	 * @var bool
	 */
	protected $success;

	/**
	 * @var String[]
	 */
	protected $notifications;

	/**
	 * @var mixed
	 */
	protected $responseInfo;

	/**
	 * @param mixed $responseArray
	 * @param null| Request $request
	 * @return mixed
	 */
	static function createWithArray($responseArray, $request = NULL) {
		$className = get_class();
		if($request !== NULL) {
			// Translate:
			$className = str_replace('Request', 'Response', get_class($request));
			if(!class_exists($className)) {
				$className = get_class();
			}
		}
		$obj = new $className();
		$obj->setResponseFromArray($responseArray);
		return $obj;
	}

	function __construct() {
		$this->success = NULL;
		$this->notifications = array();
	}

	protected function setResponseFromArray($responseArray) {
		if(!isset($responseArray['success'])) {
			$this->success = false;
			$this->notifications[] = 'invalid response';
			return;
		}

		$this->success = ($responseArray['success'] == true);
		if(!$this->success) {
			$this->notifications = $responseArray['notifications']['notification'];
			return;
		}

		$this->responseInfo = $responseArray;
	}

	/**
	 * Is the request ok?
	 * @return bool|null
	 */
	public function getIsSuccessFull() {
		return $this->success;
	}

	/**
	 * @return array|\String[]
	 */
	public function getNotifications() {
		return $this->notifications;
	}

}