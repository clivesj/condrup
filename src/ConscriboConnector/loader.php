<?php

/**
 * PSR-4 compatible autoloader
 * @param $className
 */
function loadConscribo($className) {

	if(strpos($className, 'ConscriboConnector\\') !== 0) {
		return;
	}
	$className = substr($className, strlen('ConscriboConnector\\'));

	$fileName = __DIR__ . DIRECTORY_SEPARATOR . str_replace('\\', DIRECTORY_SEPARATOR , $className). '.php';
	if(file_exists($fileName)) {
		require $fileName;
	}
}

spl_autoload_register('loadConscribo');