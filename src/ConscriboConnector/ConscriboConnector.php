<?php

namespace Drupal\condrup\ConscriboConnector;

class ConscriboConnector {

	const LIVE_BASE_URL = 'https://secure.conscribo.nl/';

	//Het testdomein bevindt zich op dit moment op dezelde server en is dus gelijk. er wordt slechts een andere account aan gekoppeld.
	const TEST_BASE_URL = 'https://secure.conscribo.nl/';

	const API_VERSION = '0.20120201';

	private $curl;
	private $response;
	private $sessionId;

	public $lastError;

	/**
	 * @var Request[]
	 */
	protected $requests;

	/**
	 * @var int
	 */
	protected $sequenceId;

	/**
	 * @var String
	 */
	protected $accountName;

	/**
	 * @var bool
	 */
	protected $testMode;

	/**
	 * @var bool
	 */
	protected $debugging;

	/**
	 * @var String
	 */
	private $identifierKey;

	/**
	 * @var String
	 */
	private $passPhrase;

	/**
	 * @var boolean
	 */
	protected $initialized;

	/**
	 * @var mixed[]
	 */
	protected $jSONRequests;

	/**
	 * @var bool
	 */
	private $multiRequestMode;


	function __construct() {
		$this->sequenceId = 0;
		$this->accountName = NULL;
		$this->testMode = false;
		$this->debugging = false;
		$this->identifierKey = NULL;
		$this->passPhrase = NULL;
		$this->initialized = false;
	}

	/**
	 * The Conscribo accountname on which to login. E.g. 'janssen' if the URL in the webinterface starts with https://secure.conscribo.nl/janssen/
	 * @param $accountName
	 * @return $this
	 */
	public function setAccountName($accountName) {
		$this->accountName = $accountName;
		return $this;
	}

	/**
	 * Use the Test URL instead of a real url.
	 * @param boolean $testMode
	 * @return $this;
	 */
	public function setTestMode($testMode) {
		$this->testMode = $testMode;
		return $this;
	}

	/**
	 * If turned on, The connector will show the communication between the connector and Conscribo.
	 * @param boolean $debugging
	 * @return ConscriboConnector
	 */
	public function setDebugging($debugging) {
		$this->debugging = $debugging;
		return $this;
	}

	/**
	 * Sets the identifierKey. This is supplied to you by Conscribo when the connector is enabled.
	 *          It can also be found in the webapplication, in the menu 'Overig',
	 *          'Configuratie', click 'Open koppelingsinstellingen'.
	 * @param String $identifierKey
	 * @return $this;
	 */
	public function setIdentifierKey($identifierKey) {
		$this->identifierKey = $identifierKey;
		return $this;
	}

	/**
	 * The passPhrase of the connected Conscribo useraccount.
	 *            This is created and connected in the webapplication.
	 *            See documentation and in the webapplication, in the menu 'Overig', 'Configuratie',
	 *            click 'Open koppelingsinstellingen'.
	 * @param String $passPhrase
	 * @return $this;
	 */
	public function setPassPhrase($passPhrase) {
		$this->passPhrase = $passPhrase;
		return $this;
	}

	/**
	 * Sets the sessionId, will be automatically be set by the authentication request, and can be set externally.
	 * Will be used to identify with Conscribo.
	 * @param $sessionId
	 */
	public function setSessionId($sessionId) {
		$this->sessionId = $sessionId;
	}

	/**
	 * @param Request $request
	 * @return Response
	 */
	public function execute(Request $request) {
		$this->schedule($request);
		$this->commit();

		return $request->getResponse();
	}


	/**
	 * @param Request $request
	 * @param null|string $sequenceId Unique request nr to identify it in the request
	 * @return $request
	 */
	public function schedule(Request $request, $sequenceId = NULL) {
		if($sequenceId === NULL) {
			$sequenceId = $this->getFreeSequenceId();
		}
		$this->requests[$sequenceId] = $request;
		return $request;
	}



	public function commit() {
		$this->initialize();

		$jSONRequests = array();

		if(count($this->requests) == 0) {
			return;
		}

		if(count($this->requests) >1) {
			$this->multiRequestMode = true;
		}

		foreach($this->requests as $sequenceId => $request) {
			if($this->multiRequestMode) {
				if($request->getRequestSequence() === NULL) {
					$this->createRequestSequenceForRequest($request, $sequenceId);
				}
			}
			$jSONRequests[$sequenceId] = $request->getJSON($this);

		}

		if(!$this->multiRequestMode) {
			$jSONRequests = array('request' => reset($jSONRequests));
		} else {
			$jSONRequests = array('requests' => array('request' => $jSONRequests));
		}

		$this->doRequest($jSONRequests);
		$this->resetRequests();
	}

	private function createRequestSequenceForRequest(Request $request, $proposedSequenceId) {
		$usedRequestSequence = $proposedSequenceId;
		$add = NULL;
		while($this->getRequestByRequestSequence($usedRequestSequence . $add) !== NULL) {
			if($add === NULL) {
				$add = 0;
				$usedRequestSequence .= '_';
			}
			$add++;
		}

		$request->setRequestSequence('connect_'. $proposedSequenceId);
	}

	public function resetRequests() {
		$this->requests = array();
		$this->sequenceId = 0;
	}

	public function getFreeSequenceId() {
		while(true) {
			if(isset($this->requests[$this->sequenceId])) {
				$this->sequenceId ++;
			} else {
				break;
			}
		}
		return $this->sequenceId;
	}

	private function doRequest($jSONRequests) {

		// JSON encode the request(s) and set it in the RAW POST DATA:
		curl_setopt($this->curl, CURLOPT_POSTFIELDS, json_encode($jSONRequests));

		// Set the used API version as HTTP Header
		$headers = array('X-Conscribo-API-Version: '. ConscriboConnector::API_VERSION);

		// If present, set the sessionId as HTTP Header
		if(isset($this->sessionId)) {
			$headers[] = 'X-Conscribo-SessionId: '. $this->sessionId;
		}

		curl_setopt($this->curl, CURLOPT_HTTPHEADER, $headers);

		$response = curl_exec($this->curl);

		if($this->debugging) {

			echo "\n" . '============================================================' . "\n";
			echo '-------------------- Sent headers: -------------------------' . "\n";
			var_dump(curl_getinfo($this->curl, CURLINFO_HEADER_OUT));
			echo "\n" . '-------------------- Request: ------------------------------' . "\n";
			var_export($jSONRequests);
			echo "\n" . '------------- Raw Response From Conscribo: -----------------' . "\n";
			var_export($response);
			echo "\n\n";
		}

		$this->parseResponse($response);

		return true;
	}

	/**
	 * @return String
	 */
	public function getIdentifierKey() {
		return $this->identifierKey;
	}

	/**
	 * @return String
	 */
	public function getPassPhrase() {
		return $this->passPhrase;
	}



	protected function parseResponse($responseString) {
		$responseArray = json_decode($responseString, true);
		if($responseArray === NULL) {
			throw new \Exception('Response could not be interpreted as JSON: '. "\n". $responseString);
		}

		$responses = array();

		if(isset($responseArray['result'])) {
			$responses[] = $responseArray['result'];
		} else {
			if(isset($responseArray['results']['result'])) {
				foreach($responseArray['results']['result'] as $singleResponseArray) {
					$responses[] = $singleResponseArray;
				}
			}
		}

		foreach($this->requests as $request) {
			// find response:
			$responseArray = NULL;

			if(count($responses) == 1) {
				$responseArray = reset($responses);
			} else {
				foreach($responses as $_responseArray) {
					if(isset($_responseArray['requestSequence']) && $_responseArray['requestSequence'] == $request->getRequestSequence()) {
						$responseArray = $_responseArray;
						break;
					}
				}
			}
			if($responseArray === NULL) {
				throw new \Exception('Partial /no response received');
			}

			$response = Response::createWithArray($responseArray, $request);
			$request->setResponse($response);
		}

	}

	/**
	 * Find a request by requestSequence. Return NULL if not found
	 * @param $requestSequence
	 * @return Request|NULL
	 */
	public function getRequestByRequestSequence($requestSequence) {
		foreach($this->requests as $request) {
			if($request->getRequestSequence() == $requestSequence) {
				return $request;
			}
		}
		return NULL;
	}

	protected function initialize() {
		if(empty($this->accountName)) {
			throw new \Exception('No Accountname set. @see setAccountName');
		}

		/**if(empty($this->identifierKey)) {
			throw new \Exception('No identifierKey set. @see setIdentifierKey');
		}
		if(empty($this->passPhrase)) {
			throw new \Exception('No passPhrase set. @see setPassPhrase');
		}*/

		if($this->initialized) {
			return;
		}

		$this->curl = curl_init();
		if($this->testMode) {
			$url = ConscriboConnector::TEST_BASE_URL . $this->accountName .'/request.json';
		} else {
			$url = ConscriboConnector::LIVE_BASE_URL . $this->accountName .'/request.json';
		}

		curl_setopt($this->curl, CURLOPT_URL, $url);
		curl_setopt($this->curl, CURLOPT_POST, true);
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($this->curl, CURLINFO_HEADER_OUT, true);

		$this->initialized = true;
	}

	public function listVatCodes($date) {
		$request = array('command' => 'listVatCodes',
						 'date' => $date);
		$requestXML = $this->arrayToXMLDocument($request);
		$this->doRequest($requestXML);
		if($this->response['success'] == 0) {
			throw new Exception(implode("\n", $this->readXMLListItemFromArray($this->response['notifications'], 'notification')));
		}
		return $this->response['vatCodes'];
	}





}
