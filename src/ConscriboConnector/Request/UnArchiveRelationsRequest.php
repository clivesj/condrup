<?php
/**
 * User: Dre
 * Date: 30-11-2016
 * Time: 17:09
 */

namespace Drupal\condrup\ConscriboConnector\Request;

use Drupal\condrup\ConscriboConnector\ConscriboConnector;
use Drupal\condrup\ConscriboConnector\Request;

class UnArchiveRelationsRequest extends Request {

	private $archiveGroupId;
	private $relationIds;

	function __construct() {
		parent::__construct();
	}

	/**
	 * Enter a group id where the relation should be unarchived from.
	 * @param int $archiveGroupId
	 */
	public function setArchiveGroupId(int $archiveGroupId) {
		$this->archiveGroupId = $archiveGroupId;
		return $this;
	}

	/**
	 * Relation who will be unarchived.
	 * @param array $relationIds
	 */
	public function setRelationIds(array $relationIds) {
		$this->relationIds = $relationIds;
		return $this;
	}


	protected function getJSONRequest(ConscriboConnector $connector) {
		$request = array(
			'command' => 'UnArchiveRelations',
			'relationIds' => $this->relationIds,
			'archiveGroupId' => $this->archiveGroupId
		);

		return $request;
	}

}