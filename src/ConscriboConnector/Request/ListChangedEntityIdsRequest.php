<?php
/**
 * User: Dre
 * Date: 30-11-2016
 * Time: 17:09
 */

namespace Drupal\condrup\ConscriboConnector\Request;

use Drupal\condrup\ConscriboConnector\ConscriboConnector;
use Drupal\condrup\ConscriboConnector\Request;

class ListChangedEntityIdsRequest extends Request {

	private $timestamp;

	function __construct() {
		parent::__construct();
	}

	/**
	 * Enter a timestamp to get all relations who are changed since then
	 * @param int $timestamp
	 */
	public function setTimestamp(int $timestamp) {
		$this->timestamp = $timestamp;
	}


	protected function getJSONRequest(ConscriboConnector $connector) {
		$request = array(
			'command' => 'listChangedEntityIds',
			'timestamp' => $this->timestamp
		);

		return $request;
	}

}