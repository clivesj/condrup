<?php
/**
 * User: Dre
 * Date: 11-12-2016
 * Time: 10:20
 */

namespace Drupal\condrup\ConscriboConnector\Request;

use Drupal\condrup\ConscriboConnector\ConscriboConnector;
use Drupal\condrup\ConscriboConnector\Request;

Class AddChangeTransactionRequest extends Request {


	private $date;

	private $description;

	private $transactionRows;

	private $mode;

	private $transactionId;

	public function __construct() {
		parent::__construct();
		$this->transactionRows = array();
		$this->mode = 'new';
	}

	public function setModeNew() {
		$this->mode = 'new';
		$this->transactionId = NULL;
		return $this;
	}

	public function setModeEdit($transactionId) {
		$this->mode = 'edit';
		$this->transactionId = $transactionId;
		return $this;
	}
	/**
	 * @param mixed $date
	 * @return AddChangeTransactionRequest
	 */
	public function setDate($date) {
		$this->date = $date;
		return $this;
	}

	/**
	 * @param mixed $description
	 * @return AddChangeTransactionRequest
	 */
	public function setDescription($description) {
		$this->description = $description;
		return $this;
	}

	/**
	 * @param string $accountNr nr of the account, returned by listAccounts
	 * @param float $creditAmount amount in the row excluding vat. Total of allRows needs to add to 0.
	 * @param null|string $reference
	 * @param null|string $relationNr nr of a relation (returned by listRelations)
	 * @param null |string $vatCode code fo the vatGroup
	 * @param null |float $vatAmount amount of vat. if vatGroup is supplied this needs to be supplied as well.
	 * @return $this
	 */
	public function addTransactionRow($accountNr, $creditAmount, $reference = NULL, $relationNr = NULL, $vatCode = NULL, $vatCreditAmount = NULL) {
		$transactionRow = array('accountNr' => $accountNr,
								'amount' => abs($creditAmount),
								'side' => ($creditAmount >= 0)? 'credit' : 'debet');

		if($this->mode == 'edit' && $this->transactionId !== NULL) {
			$transactionRow['transactionId'] = $this->transactionId;
		}

		if($reference !== NULL) {
			$transactionRow['reference'] = $reference;
		}
		if($relationNr !== NULL) {
			$transactionRow['relationNr'] = $relationNr;
		}

		if($vatCode !== NULL) {
			$transactionRow['vatCode'] = $vatCode;
		}
		if($vatCreditAmount !== NULL) {
			$vatAmount = $vatCreditAmount;
			if($transactionRow['side'] == 'debet') {
				$vatAmount = 0 - $vatCreditAmount;
			}
			$transactionRow['vatAmount'] = $vatAmount;
		}
		$this->transactionRows[] = $transactionRow;
		return $this;
	}


	protected function getJSONRequest(ConscriboConnector $connector) {
		$request = array('command' => 'addChangeTransaction',
						'date'=> $this->date,
						'description' => $this->description,
						'transactionRows' => array('transactionRow' =>$this->transactionRows));
		return $request;
	}
}