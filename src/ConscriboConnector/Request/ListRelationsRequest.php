<?php
/**
 * User: Dre
 * Date: 8-12-2016
 * Time: 16:20
 */

namespace Drupal\condrup\ConscriboConnector\Request;

use Drupal\condrup\ConscriboConnector\ConscriboConnector;
use Drupal\condrup\ConscriboConnector\Request;

Class ListRelationsRequest extends Request {


	private $entityType;

	private $fieldNames;

	private $relationNrs;

	private $limit;

	private $offset;

	private $filters;

	public function __construct() {
		parent::__construct();
		$this->filters = array();
		$this->fieldNames = array();
	}

	/**
	 * For which entityType are we requesting the fieldDefinitions
	 * @param string $entityType
	 * @return ListRelationsRequest
	 */
	public function setEntityType($entityType) {
		$this->entityType = $entityType;
		return $this;
	}

	/**
	 * Which fieldNames to request
	 * @param string[] $fieldNames
	 * @return ListRelationsRequest
	 */
	public function setFieldNames($fieldNames) {
		$this->fieldNames = $fieldNames;
		return $this;
	}

	/**
	 * Filter relationNrs (field 'code')
	 * @param string[]|int[] $relationNrs
	 * @return ListRelationsRequest
	 */
	public function setRelationNrs($relationNrs) {
		$this->relationNrs = $relationNrs;
		return $this;
	}

	/**
	 * // how many results should be returned?
	 * @param int $limit
	 * @return ListRelationsRequest
	 */
	public function setLimit($limit) {
		$this->limit = $limit;
		return $this;
	}
	/**
	 * From which offset should we read
	 * @param int $offset
	 * @return ListRelationsRequest
	 */
	public function setOffset($offset) {
		$this->offset = $offset;
		return $this;
	}


	/**
	 * @param string $fieldName	(word/ text, emailadres, bankaccount)
	 * @param string $operator one of = (equals), ~ (like), !~ (not like), |= (starts with), + (not empty), - (empty)
	 * @param string $searchValue
	 * @return ListRelationsRequest
	 */
	public function addTextFilter($fieldName, $operator, $searchValue) {
		$this->filters[] = array('fieldName' => $fieldName,
								 	'operator' => $operator,
								 	'value' => $searchValue);
		return $this;
	}

	/**
	 * @param string $fieldName
	 * @param string $operator one of >< (between), >= (from), <= (until)
	 * @param string $startDate (yyyy-mm-dd) (used with operator <> and >=)
	 * @param string $endDate (yyyy-mm-dd) (used with operator <> and <=)
	 * @return ListRelationsRequest
	 */
	public function addDateFilter($fieldName, $operator, $startDate, $endDate) {
		$this->filters[] = array('fieldName' => $fieldName,
								 'operator' => $operator,
								 'value' => array('start' => $startDate,
								 					'stop' => $endDate));
		return $this;
	}

	/**
	 * @param string $fieldName
	 * @param string $searchPhrase (See Reference)  2, >2, <3&>4, 3|>7.3
	 * @return ListRelationsRequest
	 */
	public function addNumberFilter($fieldName, $searchPhrase) {
		$this->filters[] = array('fieldName' => $fieldName,
								 'operator' => '=',
								 'value' => $searchPhrase);
		return $this;
	}

	/**
	 * @param string $fieldName
	 * @param bool $toggle
	 * @return ListRelationsRequest
	 */
	public function addCheckboxFilter($fieldName, $toggle) {
		$this->filters[] = array('fieldName' => $fieldName,
								 'operator' => '=',
								 'value' => ($toggle)? 1: 0);
		return $this;
	}

	/**
	 * @param string $fieldName
	 * @param string $operator = (equals), in (has one or more elements of), all (has all elements of), <> (has no elements of)
	 * @param int $bitMask binary options (See reference)  Option 2,5,6: 2 + 32 + 64 = 98
	 * @return ListRelationsRequest
	 */
	public function addMultiCheckboxFilter($fieldName, $operator, $bitMask) {
		$this->filters[] = array('fieldName' => $fieldName,
								 'operator' => $operator,
								 'value' => $bitMask);
		return $this;
	}

	/**
	 * @param String $fieldName
	 * @param String $value Which option
	 * @return ListRelationsRequest
	 */
	public function addEnumFilter($fieldName, $value) {
		$this->filters[] = array('fieldName' => $fieldName,
								 'operator' => '=',
								 'value' => $value);
		return $this;
	}


	protected function getJSONRequest(ConscriboConnector $connector) {

		if(!in_array('code', $this->fieldNames)) {
			$this->fieldNames[] = 'code';
		}
		$request = array('command' => 'listRelations',
						 'requestedFields' => array('fieldName' => $this->fieldNames));

		if($this->entityType !== NULL) {
			$request['entityType'] = $this->entityType;
		} else {
			// search in filters for a filter on entityType, and apply it in the correct place.
			foreach($this->filters as $filter) {
				if($filter['fieldName'] == 'entityType' && $filter['operator'] == '=') {
					$request['entityType'] = $filter['value'];
				}
			}
		}

		if($this->relationNrs !== NULL) {
			$request['codes'] = $this->relationNrs;
		}
		if($this->limit !== NULL) {
			$request['limit'] = (int)$this->limit;
		}
		if($this->offset !== NULL) {
			$request['offset'] = (int)$this->offset;
		}

		if(count($this->filters) > 0) {
			$request['filters'] = array('filter' => $this->filters);
		}
		return $request;



		$resultCount = $this->response['resultCount'];
		$relations = $this->readXMLListItemFromArray($this->response['relations'], 'relation');
		return array($resultCount, $relations);
	}
}