<?php
/**
 * User: Dre
 * Date: 8-12-2016
 * Time: 16:20
 */

namespace Drupal\condrup\ConscriboConnector\Request;

use Drupal\condrup\ConscriboConnector\ConscriboConnector;
use Drupal\condrup\ConscriboConnector\Request;

Class ListEntityTypesRequest extends Request {
	protected function getJSONRequest(ConscriboConnector $connector) {
		$request = array('command' => 'listEntityTypes');
		return $request;
	}
}