<?php
/**
 * User: Dre
 * Date: 8-12-2016
 * Time: 16:20
 */

namespace Drupal\condrup\ConscriboConnector\Request;

use Drupal\condrup\ConscriboConnector\ConscriboConnector;
use Drupal\condrup\ConscriboConnector\Request;

Class AddPhotoAppMessageRequest extends Request {

	/**
	 * @var String
	 */
	private $relationCode;

	private $accountNr;

	private $dateTime;

	private $remarks;

	private $amount;

	private $bankAccount;

	private $filePath;

	private $fileName;
	/**
	 * @param String $relationNr
	 * @return $this
	 */
	public function setRelationNr($relationNr) {
		$this->relationNr = $relationNr;
		return $this;
	}

	protected function getJSONRequest(ConscriboConnector $connector) {


		$request = array('command' => 'AddPhotoAppMessage',
			'dateTime' => $this->dateTime,
			'accountNr'=> $this->accountNr,
			'relationCode'=> $this->relationCode,
			'amount' => $this->amount,
			'bankAccount' => $this->bankAccount,
			'remarks' => $this->remarks,
			'attachment' => array('fileName' => $this->fileName,
								  'contents' => base64_encode(file_get_contents($this->filePath))));

		return $request;

	}

	/**
	 * @param String $relationCode
	 */
	public function setRelationCode(String $relationCode) {
		$this->relationCode = $relationCode;
	}

	/**
	 * @param mixed $accountNr
	 */
	public function setAccountNr($accountNr) {
		$this->accountNr = $accountNr;
	}

	/**
	 * @param mixed $dateTime
	 */
	public function setDateTime($dateTime) {
		$this->dateTime = $dateTime;
	}

	/**
	 * @param mixed $remarks
	 */
	public function setRemarks($remarks) {
		$this->remarks = $remarks;
	}

	/**
	 * @param $amount
	 */
	public function setAmount($amount) {
		$this->amount = intval($amount);
	}

	/**
	 * @param $iban
	 * @param $bic
	 * @param $name
	 */
	public function setBankAccount($iban, $bic, $name) {
		$this->bankAccount = array('iban' => $iban,
									'bic' => $bic,
									'name' => $name);
	}

	/**
	 * @param string $filePath location on fileSystem of the file
	 * @param string $fileName fileName to use as name (or NULL if equal to filename in path)
	 * @throws \Exception
	 */
	public function setFile($filePath, $fileName = NULL) {
		if($fileName === NULL) {
			$fileName = substr($filePath, strrpos($filePath,DIRECTORY_SEPARATOR)+1);
		}
		if(empty($fileName)) {
			throw new \Exception('FileName canot be empty');
		}
		if(!file_exists($filePath)) {
			throw new \Exception('File \''. $filePath .'\' not found');
		}
		$this->filePath = $filePath;
		$this->fileName = $fileName;
	}


}