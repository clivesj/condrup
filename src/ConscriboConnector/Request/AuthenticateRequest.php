<?php

namespace Drupal\condrup\ConscriboConnector\Request;


use Drupal\condrup\ConscriboConnector\ConscriboConnector;
use Drupal\condrup\ConscriboConnector\Request;
use Drupal\condrup\ConscriboConnector\Response;
use Drupal\condrup\ConscriboConnector\Response\AuthenticateResponse;

Class AuthenticateRequest extends Request {

	/**
	 * @var ConscriboConnector
	 */
	protected $connector;


	protected function getJSONRequest(ConscriboConnector $connector) {
		$this->connector = $connector;
		return array('command' => 'authenticate',
						 'apiIdentifierKey' => $connector->getIdentifierKey(),
						 'passPhrase' => $connector->getPassPhrase());
	}

	/**
	 * @param AuthenticateResponse $response
	 */
	public function setResponse(Response $response) {
		parent::setResponse($response);
		if($this->response->getIsSuccessFull()) {
			$this->connector->setSessionId($response->getSessionId());
		}
	}
}