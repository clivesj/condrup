<?php
/**
 * User: Dre
 * Date: 11-12-2016
 * Time: 10:20
 */

namespace Drupal\condrup\ConscriboConnector\Request;

use Drupal\condrup\ConscriboConnector\ConscriboConnector;
use Drupal\condrup\ConscriboConnector\Request;

Class ListTransactionsRequest extends Request {

	private $filters;

	private $limit;

	private $offset;


	public function addDateStartFilter($dateStart) {
		$this->filters['dateStart'] = $dateStart;
	}

	public function addDateEndFilter($dateEnd) {
		$this->filters['dateEnd'] = $dateEnd;
	}

	/**
	 * @param int[] $ids
	 */
	public function addTransactionIdFilter($ids) {
		$this->filters['transactionIds'] = array('id' => $ids);
	}

	/**
	 * @param string $reference
	 */
	public function addReferenceFilter($reference) {
		$this->filters['references'] = array('reference' => array($reference));
	}

	/**
	 * @param string[] $references
	 */
	public function addReferencesFilter($references) {
		$this->filters['references'] = array('reference' => $references);
	}

	/**
	 * @param string[] $relationNrs
	 */
	public function addRelationsFilter($relationNrs) {
		$this->filters['relations'] =array('relationNr' => $relationNrs);
	}

	/**
	 * @param string $accountNr
	 */
	public function addAccountNrFilter($accountNr) {
		$this->filters['accounts'] =  array('accountNr' => array($accountNr));
	}

	/**
	 * @param string[] $accountNrs
	 */
	public function addAccountNrsFilter($accountNrs) {
		$this->filters['accounts'] = array('accountNr' => $accountNrs);
	}


	public function setLimit($limit) {
		$this->limit = $limit;
	}

	public function setOffset($offset) {
		$this->offset = $offset;
	}

	protected function getJSONRequest(ConscriboConnector $connector) {
		$request = array('command' => 'listTransactions',
			'filters' => $this->filters);

		if($this->limit !== NULL) {
			$request['limit'] = $this->limit;
		}
		if($this->offset !== NULL) {
			$request['offset'] = $this->offset;
		}

		return $request;
	}
}