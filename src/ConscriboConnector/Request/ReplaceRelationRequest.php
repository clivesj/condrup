<?php
/**
 * User: Dre
 * Date: 8-12-2016
 * Time: 16:20
 */

namespace Drupal\condrup\ConscriboConnector\Request;

use Drupal\condrup\ConscriboConnector\ConscriboConnector;
use Drupal\condrup\ConscriboConnector\Request;

Class ReplaceRelationRequest extends Request {

	/**
	 * @var mixed[]
	 */
	private $changes;

	/**
	 * @var string 'new' | 'update'
	 */
	private $mode;

	/**
	 * @var String
	 */
	private $currentRelationNr;

	/**
	 * @var string
	 */
	private $keyFieldName;

	public function setKeyFieldName($keyFieldName) {
		$this->keyFieldName = $keyFieldName;
	}

	public function setModeUpdateRelation($relationNr) {
		$this->mode = 'update';
		$this->currentRelationNr = $relationNr;
		return $this;
	}

	public function setModeNewRelation() {
		$this->mode = 'new';
		$this->currentRelationNr = NULL;
		return $this;
	}

	public function setValue($fieldName, $newValue) {
		$this->changes[$fieldName] = $newValue;
	}


	protected function getJSONRequest(ConscriboConnector $connector) {
		$request = array('command' => 'replaceRelation',
			'fields' => $this->changes);

		if($this->mode =='update') {
			if($this->keyFieldName !== NULL) {
				$request['keyFieldName'] = $this->keyFieldName;
				$request[$this->keyFieldName] = $this->currentRelationNr;
			} else {
				$request['code'] = $this->currentRelationNr;
			}
		}
		return $request;

	}
}