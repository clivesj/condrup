<?php
/**
 * User: Dre
 * Date: 8-12-2016
 * Time: 16:20
 */

namespace Drupal\condrup\ConscriboConnector\Request;

use Drupal\condrup\ConscriboConnector\ConscriboConnector;
use Drupal\condrup\ConscriboConnector\Request;

Class ListFieldDefinitionsRequest extends Request {

	private $entityType;

	/**
	 * For which entityType are we requesting the fieldDefinitions
	 * @param string $entityType
	 * @return ListFieldDefinitionsRequest
	 */
	public function setEntityType($entityType) {
		$this->entityType = $entityType;
		return $this;
	}

	protected function getJSONRequest(ConscriboConnector $connector) {
		$request = array('command' => 'listFieldDefinitions');
		if($this->entityType !== NULL) {
			$request['entityType'] = $this->entityType;
		}
		return $request;
	}
}