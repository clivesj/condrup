<?php
/**
 * User: Dre
 * Date: 30-11-2016
 * Time: 17:09
 */

namespace Drupal\condrup\ConscriboConnector\Request;

use Drupal\condrup\ConscriboConnector\ConscriboConnector;
use Drupal\condrup\ConscriboConnector\Request;

class GroupAddRelationsRequest extends Request {

	private $groupId;
	private $relationIds;

	function __construct() {
		parent::__construct();
	}

	/**
	 * Enter a group id where the relation should be archived to.
	 * @param int $groupId
	 */
	public function setGroupId(int $groupId) {
		$this->groupId = $groupId;
		return $this;
	}

	/**
	 * Relation who will be archived.
	 * @param array $relationIds
	 */
	public function setRelationIds(array $relationIds) {
		$this->relationIds = $relationIds;
		return $this;
	}


	protected function getJSONRequest(ConscriboConnector $connector) {
		$request = array(
			'command' => 'GroupAddRelations',
			'relationIds' => $this->relationIds,
			'groupId' => $this->groupId
		);

		return $request;
	}

}