<?php
/**
 * User: Dre
 * Date: 30-11-2016
 * Time: 17:09
 */

namespace Drupal\condrup\ConscriboConnector\Request;

use Drupal\condrup\ConscriboConnector\ConscriboConnector;
use Drupal\condrup\ConscriboConnector\Request;

class ListEntityGroupsRequest extends Request {

	private $groupId;

	function __construct() {
		parent::__construct();
	}

	/**
	 * Enter a group id to get all details from a group.
	 * When nothing is entered, all groups will be requested
	 * @param int $timestamp
	 */
	public function setGroupId(int $groupId) {
		$this->groupId = $groupId;
	}


	protected function getJSONRequest(ConscriboConnector $connector) {
		$request['command'] = 'listEntityGroups';
		if(isset($this->groupId)) {
			$request['groupId'] = $this->groupId;
		}

		return $request;
	}

}