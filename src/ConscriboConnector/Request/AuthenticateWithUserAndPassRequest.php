<?php

namespace Drupal\condrup\ConscriboConnector\Request;


use Drupal\condrup\ConscriboConnector\ConscriboConnector;
use Drupal\condrup\ConscriboConnector\Request;
use Drupal\condrup\ConscriboConnector\Response;
use Drupal\condrup\ConscriboConnector\Response\AuthenticateResponse;

Class AuthenticateWithUserAndPassRequest extends Request {

	/**
	 * @var ConscriboConnector
	 */
	protected $connector;

	/**
	 * @var string Username
	 */
	private $userName;

	/**
	 * @var string Passphrase
	 */
	private $passPhrase;

	/**
	 * @param string $userName
	 * @return AuthenticateWithUserAndPassRequest
	 */
	public function setUserName(string $userName): AuthenticateWithUserAndPassRequest {
		$this->userName = $userName;
		return $this;
	}

	/**
	 * @param string $passPhrase
	 * @return AuthenticateWithUserAndPassRequest
	 */
	public function setPassPhrase(string $passPhrase): AuthenticateWithUserAndPassRequest {
		$this->passPhrase = $passPhrase;
		return $this;
	}

	/**
	 * @param ConscriboConnector $connector
	 * @return array
	 */
	protected function getJSONRequest(ConscriboConnector $connector) {
		$this->connector = $connector;
		return array('command' => 'authenticateWithUserAndPass',
						 'userName' => $this->userName,
						 'passPhrase' => $this->passPhrase);
	}

	/**
	 * @param AuthenticateResponse $response
	 */
	public function setResponse(Response $response) {
		parent::setResponse($response);
		if($this->response->getIsSuccessFull()) {
			$this->connector->setSessionId($response->getSessionId());
		}
	}
}