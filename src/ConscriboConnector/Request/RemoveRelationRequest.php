<?php
/**
 * User: Dre
 * Date: 8-12-2016
 * Time: 16:20
 */

namespace Drupal\condrup\ConscriboConnector\Request;

use Drupal\condrup\ConscriboConnector\ConscriboConnector;
use Drupal\condrup\ConscriboConnector\Request;

Class RemoveRelationRequest extends Request {

	/**
	 * @var String
	 */
	private $relationNr;

	/**
	 * @param String $relationNr
	 * @return $this
	 */
	public function setRelationNr($relationNr) {
		$this->relationNr = $relationNr;
		return $this;
	}

	protected function getJSONRequest(ConscriboConnector $connector) {
		$request = array('command' => 'deleteRelation',
			'code'=> $this->relationNr);

		return $request;

	}
}