<?php
/**
 * User: Dre
 * Date: 11-12-2016
 * Time: 10:20
 */

namespace Drupal\condrup\ConscriboConnector\Request;

use Drupal\condrup\ConscriboConnector\ConscriboConnector;
use Drupal\condrup\ConscriboConnector\Request;

Class ListVatCodesRequest extends Request {


	private $date;


	function __construct() {
		parent::__construct();
		$this->date = date('Y-m-d');
	}

	/**
	 * Vatcodes have an availability based upon the date. E.g. some vatcodes become available after some date or stop being available after a date.
	 * If no date is set, today is used
	 * @param String $date
	 *
	 */
	public function setDate($date) {
		$this->date = $date;
	}


	protected function getJSONRequest(ConscriboConnector $connector) {
		$request = array('command' => 'listVatCodes',
			'date' => $this->date);

		return $request;
	}
}