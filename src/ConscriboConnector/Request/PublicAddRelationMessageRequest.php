<?php
/**
 * User: Dre
 * Date: 8-12-2016
 * Time: 16:20
 */

namespace Drupal\condrup\ConscriboConnector\Request;

use Drupal\condrup\ConscriboConnector\ConscriboConnector;
use Drupal\condrup\ConscriboConnector\Request;

Class PublicAddRelationMessageRequest extends Request {

	/**
	 * @var mixed[]
	 */
	private $values;


	private $title;

	private $entityTypeName;

	public function __construct() {
		parent::__construct();
		$this->title = 'Nieuw lid';
	}


	public function setModeUpdateRelation($relationNr) {
		$this->mode = 'update';
		return $this;
	}

	public function setModeNewRelation() {
		$this->mode = 'new';
		return $this;
	}

	public function setEntityTypeName($entityTypeName) {
		$this->entityTypeName = $entityTypeName;
		return $this;
	}

	public function setValue($fieldName, $newValue) {
		$this->values[$fieldName] = $newValue;
		return $this;
	}

	public function setTitle($title) {
		$this->title = $title;
		return $this;
	}


	protected function getJSONRequest(ConscriboConnector $connector) {
		$request = array('command' => 'publicAddRelationMessage',
			'fields' => $this->values,
			'entityTypeName' => $this->entityTypeName,
			'title' => $this->title);

		return $request;

	}
}