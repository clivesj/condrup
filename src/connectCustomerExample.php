<?php
/**
 * Conscribo Connector integration example with >= PHP 5.6
 * Requires: cURL
 * @author  André de Jong for 'Conscribo online boekhouden'
 * @version 2018-01-03
 *
 * This example shows communication with Conscribo using the API.
 * It uses the PHP library ConscriboConnector which uses the JSON API implementation as described in the documentation:
 *
 **/

require_once('./ConscriboConnector/loader.php');

try {

	// Create a ConscriboConnector instance:

	$connector = new \ConscriboConnector\ConscriboConnector();

	// Set the connector configuration:
	$connector->setAccountName('<domain>')
		->setTestMode(true)
		->setDebugging(true);

	// REQUESTS:

	// Authentication:
	$authenticate = \ConscriboConnector\Request\AuthenticateWithUserAndPassRequest::create()
			->setUserName('<userName>')
			->setPassPhrase('<passWord>');

	$response = $connector->execute($authenticate);

	if(!$response->getIsSuccessfull()) {
		echo 'Unable to login: ' . implode("\n", $response->getNotifications());
	}

	echo 'Logged in' . "\n";

	// RELATION RELATED REQUESTS //
	runRelationExample($connector);

	// FINANCIAL RELATED REQUESTS //

	runFinancialExamples($connector);


} catch(Exception $e) {
	echo $e->getMessage();
}


function runRelationExample(\ConscriboConnector\ConscriboConnector $connector) {
	/**
	 * @var \ConscriboConnector\Response\ListEntityTypesResponse $entityTypes
	 */
	$entityTypes = $connector->execute(\ConscriboConnector\Request\ListEntityTypesRequest::create());

	echo 'Existing entityTypes: ' . implode(', ', $entityTypes->getEntityTypeNames()) . "\n\n";

	// LIST FIELDDEFINITIONS
	/**
	 * @var \ConscriboConnector\Response\ListFieldDefinitionsResponse $fieldDefinitions
	 */
	$fieldDefinitions = $connector->execute(\ConscriboConnector\Request\ListFieldDefinitionsRequest::create()
		->setEntityType('persoon'));

	echo 'Read ' . $fieldDefinitions->count() . ' fieldDefinitions' . "\n\n";

	// LIST RELATIONS
	/**
	 * @var \ConscriboConnector\Request\ListRelationsRequest   $relationsRequest
	 * @var \ConscriboConnector\Response\ListRelationsResponse $relations
	 */
	$relationsRequest = \ConscriboConnector\Request\ListRelationsRequest::create();
	$relationsRequest->setFieldNames($fieldDefinitions->getFieldNames())
		->addTextFilter('naam', '~', 'e')// Find everyone with an e in the surname
		->setLimit(5);

	$relations = $connector->execute($relationsRequest);
	echo 'Retreived ' . $relations->count() . ' of ' . $relations->getTotalResultCount() . ' found relations' . "\n";

	foreach($relations->getRelations() as $relation) {
		echo 'Received: ' . $relation['selector'] . "\n";
	}

	// NEW RELATION

	/**
	 * @var \ConscriboConnector\Request\ReplaceRelationRequest $newRelationRequest
	 */
	$newRelationRequest = \ConscriboConnector\Request\ReplaceRelationRequest::create();
	$newRelationRequest->setModeNewRelation();

	foreach($fieldDefinitions->getFieldNames() as $fieldName) {
		if($fieldDefinitions->getFieldType($fieldName) == 'string'
			&& !$fieldDefinitions->getFieldIsReadOnly($fieldName)
		) {
			$newRelationRequest->setValue($fieldName, str_rot13($fieldName));
		}
	}

	$newRelationRequest->setValue('naam', 'Piet');
	/**
	 * @var \ConscriboConnector\Response\ReplaceRelationResponse $newRelation
	 */
	$newRelation = $connector->execute($newRelationRequest);
	echo 'Added relation with nr ' . $newRelation->getRelationNr() . "\n";

	// EXAMPLE UPDATE RELATION, MULTIPLEREQUESTMODE:

	$res = array();

	foreach($relations->getRelations() as $relation) {
		/**
		 * @var \ConscriboConnector\Request\ReplaceRelationRequest $updateRequest
		 */
		$updateRequest = \ConscriboConnector\Request\ReplaceRelationRequest::create();

		echo 'Changing ' . $relation['naam'] . ' to ' . str_replace('e', 'ee', $relation['naam']) . "\n";
		$updateRequest->setModeUpdateRelation($relation['code'])
			->setValue('naam', str_replace('e', 'ee', $relation['naam']));    // update naam, change all 'e' to dubble 'ee'
		$res[$relation['code']] = $connector->schedule($updateRequest);
	}

	$connector->commit();

	foreach($res as $relationNr => $request) {
		/**
		 * @var \ConscriboConnector\Request\ReplaceRelationRequest   $request
		 * @var \ConscriboConnector\Response\ReplaceRelationResponse $updatedRelation
		 */
		$updatedRelation = $request->getResponse();
		$relation = $relations->getRelationWithNr($relationNr);
		echo $relation['selector'] . ' changed' . "\n";
	}

	// REMOVE RELATION

	$removeRelationRequest = \ConscriboConnector\Request\RemoveRelationRequest::create();
	/**
	 * @var \ConscriboConnector\Request\RemoveRelationRequest $removeRelationRequest
	 */
	$removeRelationRequest->setRelationNr($newRelation->getRelationNr());
	$response = $connector->execute($removeRelationRequest);

	if($response->getIsSuccessFull()) {
		echo 'Relation ' . $newRelation->getRelationNr() . ' removed' . "\n";
	}

}


function runFinancialExamples(\ConscriboConnector\ConscriboConnector $connector) {

	$newTransaction = ConscriboConnector\Request\AddChangeTransactionRequest::create();
	/**
	 * @var \ConscriboConnector\Request\AddChangeTransactionRequest $newTransaction
	 */
	$newTransaction->setModeNew()
		->setDate(date('Y-m-d'))
		->setDescription('One transaction')
		->addTransactionRow('1000', 100, 'F-1')
		->addTransactionRow('4000', -50, 'F-1')
		->addTransactionRow('8000', -50, 'F-1');

	$resp = $connector->execute($newTransaction);

	if($resp->getIsSuccessFull()) {
		echo 'Transactie met id ' . $resp->getTransactionId() . " toegevoegd.\n";
	}


	// LIST TRANSACTIONS:
	$transactions = \ConscriboConnector\Request\ListTransactionsRequest::create();
	/**
	 * @var \ConscriboConnector\Request\ListTransactionsRequest $transactions
	 */

	$transactions->addDateStartFilter(date('Y-m-d'));
	$transactions->addAccountNrFilter(4000);
	$transactions->addReferenceFilter('F-1');

	$result = $connector->execute($transactions);
	/**
	 * @var \ConscriboConnector\Response\ListTransactionsResponse $result ;
	 */
	if($result->getIsSuccessFull()) {
		echo 'Received ' . $result->count() . ' of ' . $result->getTotalResultCount() . ' transactions' . "\n";
		var_export($result->getTransactions());
	}

	$accountsRequest = \ConscriboConnector\Request\ListAccountsRequest::create();
	/**
	 * @var \ConscriboConnector\Request\ListAccountsRequest $accountsRequest ;
	 */

	$accounts = $connector->execute($accountsRequest);
	echo 'Received ' . $accounts->count() . ' Accounts ' . "\n";


	$vatCodesRequest = \ConscriboConnector\Request\ListVatCodesRequest::create();
	/**
	 * @var \ConscriboConnector\Request\ListVatCodesRequest $vatCodesRequest
	 */
	$vatCodes = $connector->execute($vatCodesRequest);

	echo 'Received ' . $vatCodes->count() . ' Vatcodes ' . "\n";

}



