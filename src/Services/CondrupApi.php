<?php
/**
 * Created by PhpStorm.
 * User: joop
 * Date: 10/30/19
 * Time: 9:39 PM
 */

namespace Drupal\condrup\Services;


use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Response;

class CondrupApi extends ControllerBase {

  /**
   * @var Client
   */
  protected $client;

  public function getSessionId() {
    // Check if there is a valid one in SESSION
    if (isset($_SESSION['condrup']))  {
      $sessionId = $_SESSION['condrup'];
    }
    else {
      $sessionId = $this->authenticate();
    }
    return $sessionId;
  }

  public function listMembers() {
    /**
     * @var Client $client
     */
     $client = \Drupal::service('http_client_factory')->fromOptions([
      'base_uri' => Common::BASE_URI . '/' . Common::ACCOUNT_NAME,
    ]);
    $response = $this->client->get('json.api', [],
      json_encode([
        'request' => [
          'method' => 'listEntityTypes',
        ]
      ]));

  }

  private function authenticate() {
    /** @var Client $client */
    /** @var Response $response */

    try {

      $response = \Drupal::httpClient()->post(Common::BASE_URI . '/' . Common::ACCOUNT_NAME . '/json.api', [
            'method' => 'authenticateWithUserAndPass',
            'userName' => 'webkoppeling',
            'passPhrase' => Common::API_USER_PASS,
      ]);
      $data = $response->getBody()->getContents();
     // $data = print_r($response->getHeaders());

      //$cook = unserialize($_COOKIE);
      \Drupal::messenger()->addMessage($response);

    }
    catch (RequestException $e) {
      watchdog_exception('condrup', $e->get());
      $data = 'error';
    }
    return $data;
 }


}

