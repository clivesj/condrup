<?php

/**
 * Created by PhpStorm.
 * User: joop
 * Date: 10/27/19
 * Time: 9:50 PM
 */

namespace Drupal\condrup\Services;

use Drupal\condrup\ConscriboConnector\ConscriboConnector;
use Drupal\condrup\ConscriboConnector\Request\AuthenticateWithUserAndPassRequest;
use Drupal\condrup\ConscriboConnector\Request\ListEntityGroupsRequest;
use Drupal\condrup\ConscriboConnector\Request\ListFieldDefinitionsRequest;
use Drupal\condrup\ConscriboConnector\Request\ListRelationsRequest;
use Drupal\condrup\ConscriboConnector\Request\ReplaceRelationRequest;
use Drupal\condrup\ConscriboConnector\Response;
use Drupal\condrup\ConscriboConnector\Response\ListRelationsResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\KernelTests\Core\Plugin\FactoryTest;
use Drupal\user\Entity\User;

class Common extends ControllerBase {
  //todo  Move these to config
  const BASE_URI = 'https://secure.conscribo.nl'; // https://secure.conscribo.nl/<accountName>/request.json
  CONST ACCOUNT_NAME = 'NVAV';
  const API_USER_NAME = 'webkoppeling';
  const API_USER_PASS = 'Cmni8hn3e@0511';

  // This function is called after you save a user
  public function syncUserToConscribo(User $user) {
    /**
     * @function
     * Update user Drupal >>> Conscribo
     */
    // Sync user data to Conscribo
    if (isset($user->field_conscribo_id) && (!empty($user->field_conscribo_id->value)) ) {
      $data = [];
      $cid = $user->field_conscribo_id->value;
      //collect the user fields to be sent to Conscribo:

      $fields = $this->drupalToConscriboFields();
      //debug($fields);
      foreach ($fields as $from => $to) {
        if ($from == "field_email") {
          $data[$to] = $user->getEmail();
        }
        else {
          $data[$to] = $user->$from->value;
        }

      }
      //debug($data);
      return $this->editUser($cid, $data);
    }
    else {
      \Drupal::messenger()->addWarning($this->t('@id has no Conscribo id', ['@id' => $user->getAccountName()]));
      return false;
    }
  }

  /**
   * @function Returns a list of all members on Conscribo
   * returns an array
   * [0] = [
   *   'entityId' => 123,
   *   'entityType'=> 'persoon',
   * @return array
   */

  public function verifyConscriboIds() {
    /**
     * @var ListEntityGroupsRequest $request
     * @var Response\ListEntityGroupsResponse $response
     * @var ConscriboConnector $connector
     */
    $connector = $this->createConscriboConnector();
    $request = ListEntityGroupsRequest::create();
    $response = $connector->execute($request);
    $data = $response->getGroups()['11'];
    return $data['members'];
  }

  /**
   * @function edits a Conscribo user
   * @param integer $cid  Conscribo Id to edit
   * @param array $data  data to add to Conscribo
   * @return boolean True if post succeeded else Fals
   */
  protected function editUser($cid, $data) {
    /**
     * @var \Drupal\condrup\ConscriboConnector\ConscriboConnector $connector
     * @var \Drupal\condrup\ConscriboConnector\Request\ReplaceRelationRequest $request
     * @var \Drupal\condrup\ConscriboConnector\Response\ReplaceRelationResponse $ response
     */
    $connector = $this->createConscriboConnector();
    $request = ReplaceRelationRequest::create();
    $request->setModeUpdateRelation($cid);
    foreach($data as $name => $value) {
      // We don't want to delete Conscribo data, so we don't write empty data
      if (!empty($value)) {
        $request->setValue($name, $value);
      }
    }
    $response = $connector->execute($request);
    return $response->getIsSuccessFull();
  }

  public function idFromNvav($id, $destination = 'drupal') {
    /**
     * @var  $account
     */
    $account = current(\Drupal::entityTypeManager()
      ->getStorage('user')
      ->loadByProperties(['field_oude_nvav_id_int' => intval($id)]));
    if ($account) {
      if ($destination == 'conscribo') {
        return $account->field_conscribo_id->value;
      }
      else {
        return $account->uid->value;
      }
    }
    else {
      return FALSE;
    }
  }

  public function drupalUidFromConscribo($id) {
    /**
     * @var User $account
     */
    $account = current(\Drupal::entityTypeManager()
      ->getStorage('user')
      ->loadByProperties(['field_conscribo_id' => $id]));
    if ($account) {
      return $account->get('uid')->getValue();
    }
    else {
      return FALSE;
    }
  }

  public function displayNameFromUid($uid) {
    $user = User::load($uid);
    $name = $user ? $user->field_archief_naam->value : 'Unknown';
    return $name;
  }

  public function bvlConvert($string) {
    if ($string == 'actief' || $string == 'ja' || $string == 'geldig' || $string == 'verleend' || $string == 'ok') {
      return 'geldig';
    }
    elseif ($string == 'geen') {
      return 'geen';
    }
    return 'onbekend';
  }

//  public function dateConvert($source) {
//    if (empty($source)) {
//      $source = '01-01-1980';
//    }
//    $date = date_create($source);
//    return date_format($date, "Y-m-d");
//  }

//  public function oldIdToAircraftEntity($old_id) {
//    $ids = \Drupal::entityQuery('aircraft_entity')
//      ->condition('field_old_id_int', intval($old_id))
//      ->execute();
//    return reset($ids);
//  }

  public function createConscriboConnector() {
    try {

      // Create a ConscriboConnector instance:
      $connector = new ConscriboConnector();

      // Set the connector configuration:
      $connector->setAccountName(Common::ACCOUNT_NAME)
        ->setTestMode(false)
        ->setDebugging(false);

      /**
       * @var AuthenticateWithUserAndPassRequest $authenticate
       */

      // Authentication:
      $authenticate = AuthenticateWithUserAndPassRequest::create();
      $authenticate->setUserName(Common::API_USER_NAME);
      $authenticate->setPassPhrase(Common::API_USER_PASS);

      $response = $connector->execute($authenticate);

      if (!$response->getIsSuccessfull()) {
        echo 'Unable to login: ' . implode("\n", $response->getNotifications());
      }

      //echo 'Logged in' . "\n";

      // RELATION RELATED REQUESTS //
      //$this->runRelationExample($connector);

      // FINANCIAL RELATED REQUESTS //

      // $this->runFinancialExamples($connector);
      return $connector;

    } catch (\Exception $e) {
      echo $e->getMessage();
    }
  }

  /**
   * Returns data retrieved from Conscribo
   *
   * Array [
   *   code => [
   *     label => Id
   *     value => 667
   *  ]
   * ]
   * array keys:
   * [naam_informeel]
   * [voornaam]
   * [tussenvoegsel]
   * [naam]
   * [code]
   * [adres]
   * [telefoon]
   * [mobiel]
   * [email]
   * @param integer $cid Conscribo Id
   * @return array|null
   */

  //fixme deprecate - where is used
  public function cidConscriboUserFields($cid) {
    /**
     * @var \Drupal\condrup\ConscriboConnector\Request\ListRelationsRequest $relationRequest
     * @var \Drupal\condrup\ConscriboConnector\Response\ListRelationsResponse $relationResponse
     * @var \Drupal\condrup\ConscriboConnector\Request\ListFieldDefinitionsRequest $fieldsRequest
     * @var \Drupal\condrup\ConscriboConnector\Response\ListFieldDefinitionsResponse $fieldsResponse
     * @var \Drupal\condrup\ConscriboConnector\ConscriboConnector $connector
     */

    $connector = $this->createConscriboConnector();

    $fieldsRequest = ListFieldDefinitionsRequest::create();
    $fieldsRequest->setEntityType('persoon');
    $fieldsResponse = $connector->execute($fieldsRequest);
    $fields = $fieldsResponse->getFieldNames();

    $relationRequest = new ListRelationsRequest();
    $relationRequest
      ->setFieldNames($fields)
      ->setEntityType('persoon')
      ->addNumberFilter('code', $cid);

    $relationResponse = $connector->execute($relationRequest);
    // return Null if id not found in Conscribo
    if ($relationResponse->count() == 0) {
      return null;
    }

    $result = $relationResponse->getRelationWithNr($cid);
    // we don't need all the fields
    // fixme: used in two methods
/*    $fields = [
      'selector' => 'field_archief_naam',
      'naam_informeel' => 'field_naam_informeel',
      'voornaam' => 'field_voornaam',
      'tussenvoegsel' => 'field_tussenvoegsel',
      'naam' => 'field_achternaam',
      'code' => 'field_conscribo_id',
      'adres' => 'field_adress_conscribo',
      'telefoon' => 'field_telephone',
      'mobiel' => 'field_mobile',
      'email' => 'field_email',
    ];*/

    $fields = $this->conscriboToDrupalFields();
    $data = $this->parseFields($fields, $result);
    return $data;
  }

  /**
   * Returns data retrieved from Conscribo
   *
   */
  public function getFromConscribo($cid) {
    /**
     * @var \Drupal\condrup\ConscriboConnector\Request\ListRelationsRequest $relationRequest
     * @var \Drupal\condrup\ConscriboConnector\Response\ListRelationsResponse $relationResponse
     * @var \Drupal\condrup\ConscriboConnector\Request\ListFieldDefinitionsRequest $fieldsRequest
     * @var \Drupal\condrup\ConscriboConnector\Response\ListFieldDefinitionsResponse $fieldsResponse
     * @var \Drupal\condrup\ConscriboConnector\ConscriboConnector $connector
     */

    $connector = $this->createConscriboConnector();

    // get field definitions
    // fixme remove and put in ia cache routine
//    $fieldsRequest = ListFieldDefinitionsRequest::create();
//    $fieldsRequest->setEntityType('persoon');
//    $fieldsResponse = $connector->execute($fieldsRequest);
//    $fields_to_query = $fieldsResponse->getFieldNames();

    // returns the field we are interested in
    $fields_to_query = $this->conscriboFieldsUpdateUser();

    $relationRequest = new ListRelationsRequest();
    $relationRequest
        ->setFieldNames($fields_to_query)
        ->setEntityType('persoon')
        ->addNumberFilter('code', $cid);

    $relationResponse = $connector->execute($relationRequest);
    // return Null if id not found in Conscribo
    if ($relationResponse->count() == 0) {
      return null;
    }

    $result = $relationResponse->getRelationWithNr($cid);
    // we don't need all the fields, so get an array of only the relevant fields
    // fixme: used in two methods
    $fields = $this->conscriboToDrupalFields();
    $data = $this->parseFields($fields, $result);
    return $data;
  }

  /**
   * Returns data retrieved from Conscribo to be displayed on de Drupal user page. NULL if
   * user load fails.
   * @param $uid
   * @return array|null
   */
  public function conscriboUserFields($uid) {
    /**
     * @var \Drupal\Core\Entity\EntityInterface $entity
     * @var \Drupal\condrup\Services\Common $condrup
     * @var \Drupal\nvav\Services\Common\Common
     * @var \Drupal\condrup\ConscriboConnector\Request\ListRelationsRequest $relationRequest
     * @var \Drupal\condrup\ConscriboConnector\Response\ListRelationsResponse $relationResponse
     * @var \Drupal\condrup\ConscriboConnector\Request\ListFieldDefinitionsRequest $fieldsRequest
     * @var \Drupal\condrup\ConscriboConnector\Response\ListFieldDefinitionsResponse $fieldsResponse
     * @var \Drupal\condrup\ConscriboConnector\ConscriboConnector $connector
     * @var User $account
     */

    $account = User::load($uid);
    if (!$account = User::load($uid)) {
      return FALSE;
    }
    //debug(array_keys($this->conscriboFields()), 'available fields');
    $connector = $this->createConscriboConnector();
    $conscribo_code = $account->field_conscribo_id->value;

//    $fieldsRequest = ListFieldDefinitionsRequest::create();
//    $fieldsRequest->setEntityType('persoon');
//    $fieldsResponse = $connector->execute($fieldsRequest);
//    $fields = $fieldsResponse->getFieldNames();

    // fixme replace by cached item
    $fields_to_query = $this->conscriboFieldsUpdateUser();
    $relationRequest = new ListRelationsRequest();
    $relationRequest
      ->setFieldNames($fields_to_query)
      ->setEntityType('persoon')
      ->addNumberFilter('code', $conscribo_code);

    $relationResponse = $connector->execute($relationRequest);
    $result = $relationResponse->getRelationWithNr($conscribo_code);
    // we don't need all the fields
    $fields_to_parse = $this->conscriboToDrupalFields();
    $data = $this->parseFields($fields_to_parse, $result);
   return $data;
  }

  private function parseFields($fields, $haystack) {
    $data = null;
    foreach ($fields as $key => $label) {
      $data[$key] = [
        'label' => $label,
        'value' => $haystack[$key],
        ];
    }
    return $data;
  }

  /**
   * Returns an array to be used for sync from Drupal to Conscribo
   * Key is the Drupal field, value is the Conscribo field
   *
   * Some values kan not be written from Drupal to Conscribo, they can only be changed in Conscribo
   * "Samengestelde" fields will not be written to Conscribo. Can only be changed IN conscribo.
   */
  public function drupalToConscriboFields() {
    return [
      'field_email' => 'email',
      'field_mobile' => 'telefoon',
      'field_telephone' => 'mobiel',
      'field_adres_straat' => 'straat',
      'field_adres_straat_2' => 'adres2',
      'field_adres_postcode' => 'postcode',
      'field_adres_land' => 'land',
      'field_adres_plaats' => 'plaats',
      'field_gegevens_delen' => 'gegevens_delen',
    ];
  }

  /**
   * @file provides an array with the relevant field for a sync ops.
   * array as conscribo_field => drupal_field
   *
   */
  protected function conscriboToDrupalFields() {
    return  [
      'naam' => 'field_achternaam',
      'voornaam' => 'field_voornaam',
      'code' => 'field_conscribo_id',
      'naam_informeel' => 'field_naam_informeel',
      'selector' => 'field_archief_naam',
      'adres' => 'field_adres',
      'straat' => 'field_adres_straat',
      'adres2' => 'field_adres_straat_2',
      'plaats' => 'field_adres_plaats',
      'postcode' => 'field_adres_postcode',
      'land' => 'field_adres_land',
      'gegevens_delen' => 'field_gegevens_delen',
      'telefoon' => 'field_telephone',
      'mobiel' => 'field_mobile',
      'email' => 'field_email',
    ];
  }

  /**
   * @function returns all the fields that are available from Conscribo
   */
  protected function conscriboFieldsAll() {
    return [
      'startdatum_lid',
      'einddatum_lid',
      'opmerkingen',
      'adres2',
      'melddat',
      'codksob',
      'codkosc',
      'bankplaats',
      'banknaa',
      'kontakt',
      'foto',
      'geb_datum',
      'rekening',
      'postcode',
      'straat',
      'huisnr',
      'huisnr_toev',
      'plaats',
      'telefoon',
      'mobiel',
      'email',
      'voornaam',
      'tussenvoegsel',
      'weergavenaam',
      'selector',
      'adres',
      'naam',
      'aanhef',
      'code',
      'functie',
      'type',
      'incas',
      'opmerkingen2',
      'hoenvav',
      'land',
      'nvav_nummer',
      'contributieklasse',
      'contributiebedrag',
      'betaalwijze',
      'naam_informeel',
      'gegevens_delen',
      ];
  }

  /**
   * @function return the fields that we want to retrieve when syncing a user from
   * Conscribo to Drupal
   * @return array
   */
  protected function conscriboFieldsUpdateUser() {
    return [
      'selector',
      'voornaam',
      'naam',         // achternaam
      'adres',        // samengesteld
      'adres2',
      'postcode',
      'straat',
      'plaats',
      'land',
      'telefoon',
      'mobiel',
      'email',
      'naam_informeel', // samengesteld
      'gegevens_delen',
    ];
  }

  // fixme: verify still in use
  public function conscriboFields() {
    return
      [
        'persoon' => [
          'field_name' => 'startdatum_lid',
          'label' => 'Start Lidmaatschap',
          'description' => 'Het gegeven =>  Start Lidmaatschap',
          'type' => 'date',
          'required' => FALSE,
          'readOnly' => FALSE,
          'display_field' => true,
        ],
        'einddatum_lid' => [
          'field_name' => 'einddatum_lid',
          'label' => 'Eind Lidmaatschap',
          'description' => 'Eind Lidmaatschap',
          'type' => 'date',
          'required' => FALSE,
          'readOnly' => FALSE,
        ],
        'opmerkingen' => [
          'field_name' => 'opmerkingen',
          'label' => 'Opmerkingen',
          'description' => 'Opmerkingen',
          'type' => 'textarea',
          'required' => FALSE,
          'readOnly' => FALSE,
        ],
        'adres2' => [
          'field_name' => 'adres2',
          'label' => 'ADRES2',
          'description' => 'ADRES2',
          'type' => 'text',
          'required' => NULL,
          'readOnly' => FALSE,
        ],
        'melddat' => [
          'field_name' => 'melddat',
          'label' => 'MELDDAT',
          'description' => 'MELDDAT',
          'type' => 'date',
          'required' => NULL,
          'readOnly' => FALSE,
        ],
        'codksob' => [
          'field_name' => 'codksob',
          'label' => 'CODKSOB',
          'description' => 'CODKSOB',
          'type' => 'text',
          'required' => NULL,
          'readOnly' => FALSE,
        ],
        'rekening' => [
          'field_name' => 'rekening',
          'entityType' => 'persoon',
          'label' => 'Bankrekeningnummer',
          'description' => 'Bankrekeningnummer',
          'type' => 'account',
          'required' => FALSE,
          'readOnly' => FALSE,
        ],
        'postcode' => [
          'field_name' => 'postcode',
          'label' => 'Postcode',
          'description' => 'Postcode',
          'type' => 'text',
          'required' => FALSE,
          'readOnly' => FALSE,
        ],
        'straat' => [
          'field_name' => 'straat',
          'label' => 'Straatnaam',
          'description' => 'Straatnaam',
          'type' => 'text',
          'required' => FALSE,
          'readOnly' => FALSE,
        ],
        'huisnr' => [
          'field_name' => 'huisnr',
          'label' => 'Huisnr',
          'description' => 'Huisnr',
          'type' => 'number',
          'required' => FALSE,
          'readOnly' => FALSE,
        ],
        'huisnr_toev' => [
          'field_name' => 'huisnr_toev',
          'label' => 'Huisnr toev.',
          'description' => 'Huisnr toev.',
          'type' => 'text',
          'required' => FALSE,
          'readOnly' => FALSE,
        ],
        'plaats' => [
          'field_name' => 'plaats',
          'label' => 'Plaatsnaam',
          'description' => 'Plaatsnaam',
          'type' => 'text',
          'required' => FALSE,
          'readOnly' => FALSE,
        ],
        'mobiel' => [
          'field_name' => 'mobiel',
          'label' => 'Mobiel nummer',
          'description' => 'Mobiel nummer',
          'type' => 'text',
          'required' => FALSE,
          'readOnly' => FALSE,
        ],
        'telefoon' => [
          'field_name' => 'telefoon',
          'label' => 'Telefoonnummer',
          'description' => 'Telefoonnummer',
          'type' => 'text',
          'required' => FALSE,
          'readOnly' => FALSE,
        ],
        'email' => [
          'field_name' => 'email',
          'label' => 'E-mailadres',
          'description' => 'E-mailadres',
          'type' => 'mailadres',
          'required' => FALSE,
          'readOnly' => FALSE,
        ],
        'bankplaats' => [
          'field_name' => 'bankplaats',
          'entityType' => 'persoon',
          'label' => 'BANKPLAATS',
          'description' => 'BANKPLAATS',
          'type' => 'text',
          'required' => NULL,
          'readOnly' => FALSE,
        ],
        'banknaam' => [
          'field_name' => 'banknaam',
          'label' => 'BANKNAAM',
          'description' => 'BANKNAAM',
          'type' => 'text',
          'required' => NULL,
          'readOnly' => FALSE,
        ],
        'geb_datum' => [
          'field_name' => 'geb_datum',
          'label' => 'Geboortedatum',
          'description' => 'Geboortedatum',
          'type' => 'date',
          'required' => FALSE,
          'readOnly' => FALSE,
        ],
        'codkosc' => [
          'field_name' => 'codkosc',
          'label' => 'CODKOSC',
          'description' => 'CODKOSC',
          'type' => 'text',
          'required' => NULL,
          'readOnly' => FALSE,
        ],
        'kontakt' => [
          'field_name' => 'kontakt',
          'label' => 'KONTAKT',
          'description' => 'KONTACT',
          'type' => 'text',
          'required' => NULL,
          'readOnly' => FALSE,
        ],
        'foto' => [
          'field_name' => 'foto',
          'label' => 'Foto',
          'description' => 'Foto',
          'type' => 'file',
          'required' => FALSE,
          'readOnly' => TRUE
        ],
        'voornaam' => [
          'field_name' => 'voornaam',
          'label' => 'Voornaam',
          'description' => 'Voornaam',
          'type' => 'text',
          'required' => FALSE,
          'readOnly' => FALSE
        ],
        'tussenvoegsel' => [
          'field_name' => 'tussenvoegsel',
          'label' => 'Tussenvoegsel',
          'description' => 'Tussenvoegsel',
          'type' => 'text',
          'required' => FALSE,
          'readOnly' => FALSE,
        ],
        'weergavenaam' => [
          'field_name' => 'weergavenaam',
          'label' => 'Weergavenaam',
          'description' => 'Weergavenaam',
          'type' => 'text',
          'required' => FALSE,
          'readOnly' => TRUE
        ],
        'selector' => [
          'field_name' => 'selector',
          'label' => 'Selectienaam',
          'description' => 'Selectienaam',
          'type' => 'text',
          'required' => FALSE,
          'readOnly' => TRUE
        ],
        'adres' => [
          'field_name' => 'adres',
          'label' => 'Adres',
          'description' => 'Adres',
          'type' => 'text',
          'required' => FALSE,
          'readOnly' => TRUE
        ],
        'naam' => [
          'field_name' => 'naam',
          'label' => 'Naam',
          'description' => 'Achternaam',
          'type' => 'text',
          'required' => TRUE,
          'readOnly' => FALSE
        ],
        'aanhef' => [
          'field_name' => 'aanhef',
          'label' => 'Aanhef',
          'description' => 'Aanhef',
          'type' => 'enum',
          'required' => FALSE,
          'readOnly' => FALSE,
          'possibleValues' => ['Dhr.', 'Mevr.']
        ],
        'code' => [
          'field_name' => 'code',
          'label' => 'Relatienummer',
          'description' => 'Relatienummer',
          'type' => 'text',
          'required' => TRUE,
          'readOnly' => FALSE,
        ],
        'lidopzeg' => [
          'field_name' => 'lidopzeg',
          'label' => 'LIDOPZEG',
          'description' => 'LIDOPZEG',
          'type' => 'date',
          'required' => NULL,
          'readOnly' => FALSE,
        ],
        'functie' => [
          'field_name' => 'functie',
          'label' => 'FUNCTIE',
          'description' => 'FUNCTIE',
          'type' => 'text',
          'required' => NULL,
          'readOnly' => FALSE,
        ],
        'type' => [
          'field_name' => 'type',
          'label' => 'TYPE',
          'description' => 'TYPE',
          'type' => 'text',
          'required' => NULL,
          'readOnly' => FALSE
        ],
        'contribu' => [
          'field_name' => 'contribu',
          'label' => 'CONTRIBU',
          'description' => 'CONTRIBU',
          'type' => 'text',
          'required' => NULL,
          'readOnly' => FALSE
        ],
        'contklas' => [
          'field_name' => 'contklas',
          'label' => 'CONTKLAS',
          'description' => 'CONTKLAS',
          'type' => 'text',
          'required' => NULL,
          'readOnly' => FALSE
        ],
        'incas' => [
          'field_name' => 'incas',
          'label' => 'INCAS',
          'description' => 'INCAS',
          'type' => 'text',
          'required' => NULL,
          'readOnly' => FALSE,
        ],
        'opmerkingen2' => [
          'field_name' => 'opmerkingen2',
          'label' => 'Opmerkingen2',
          'description' => 'Opmerkingen2',
          'type' => 'text',
          'required' => NULL,
          'readOnly' => FALSE,
        ],
        'hoenvav' => [
          'field_name' => 'hoenvav',
          'label' => 'Hoenvav',
          'description' => 'Hoenvav',
          'type' => 'textarea',
          'required' => NULL,
          'readOnly' => FALSE,
        ],
        'land' => [
          'field_name' => 'land',
          'label' => 'Land',
          'description' => 'Land',
          'type' => 'text',
          'required' => NULL,
          'readOnly' => FALSE,
        ],
        'actief' => [
          'field_name' => 'actief',
          'label' => 'ACTIEF',
          'description' => 'ACTIEF',
          'type' => 'text',
          'required' => NULL,
          'readOnly' => FALSE,
        ],
        'nvav_nummer' => [
          'field_name' => 'nvav_nummer',
          'label' => 'NVAV nummer',
          'description' => 'NVAV nummer',
          'type' => 'number',
          'required' => NULL,
          'readOnly' => FALSE,
        ],
      ];
  }


  public function seo_friendly_url($string) {
    $string = str_replace(array('[\', \']'), '', $string);
    $string = preg_replace('/\[.*\]/U', '', $string);
    $string = preg_replace('/&(amp;)?#?[a-z0-9]+;/i', '-', $string);
    $string = htmlentities($string, ENT_COMPAT, 'utf-8');
    $string = preg_replace('/&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);/i', '\\1', $string);
    $string = preg_replace(array('/[^a-z0-9]/i', '/[-]+/'), '-', $string);
    return strtolower(trim($string, '-'));
  }

  /**
   * @function Replaces the keys in an array with the values in the first row.
   * @param array $source
   * @return array
   */
  public function arrayReplaceKeys($source) {
    $keys = [];
    $destination = [];
    // Display results
    foreach ($source[0] as $key) {
      $keys[] = $key;
    }
    foreach ($source as $key => $result) {
      for ($x = 0; $x < count($keys); $x++) {
        $destination[$key][$keys[$x]] = $result[$x];
      }
    }
    return $destination;
  }
}
