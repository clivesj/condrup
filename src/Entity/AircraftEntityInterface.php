<?php

namespace Drupal\condrup\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Aircraft entity entities.
 *
 * @ingroup condrup
 */
interface AircraftEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Aircraft entity name.
   *
   * @return string
   *   Name of the Aircraft entity.
   */
  public function getName();

  /**
   * Sets the Aircraft entity name.
   *
   * @param string $name
   *   The Aircraft entity name.
   *
   * @return \Drupal\condrup\Entity\AircraftEntityInterface
   *   The called Aircraft entity entity.
   */
  public function setName($name);

  /**
   * Gets the Aircraft entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Aircraft entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Aircraft entity creation timestamp.
   *
   * @param int $timestamp
   *   The Aircraft entity creation timestamp.
   *
   * @return \Drupal\condrup\Entity\AircraftEntityInterface
   *   The called Aircraft entity entity.
   */
  public function setCreatedTime($timestamp);

}
