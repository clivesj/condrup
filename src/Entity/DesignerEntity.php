<?php

namespace Drupal\condrup\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Designer entity entity.
 *
 * @ingroup condrup
 *
 * @ContentEntityType(
 *   id = "designer_entity",
 *   label = @Translation("Designer entity"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\condrup\DesignerEntityListBuilder",
 *     "views_data" = "Drupal\condrup\Entity\DesignerEntityViewsData",
 *     "translation" = "Drupal\condrup\DesignerEntityTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\condrup\Form\DesignerEntityForm",
 *       "add" = "Drupal\condrup\Form\DesignerEntityForm",
 *       "edit" = "Drupal\condrup\Form\DesignerEntityForm",
 *       "delete" = "Drupal\condrup\Form\DesignerEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\condrup\DesignerEntityHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\condrup\DesignerEntityAccessControlHandler",
 *   },
 *   base_table = "designer_entity",
 *   data_table = "designer_entity_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer designer entity entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/designer_entity/{designer_entity}",
 *     "add-form" = "/admin/structure/designer_entity/add",
 *     "edit-form" = "/admin/structure/designer_entity/{designer_entity}/edit",
 *     "delete-form" = "/admin/structure/designer_entity/{designer_entity}/delete",
 *     "collection" = "/admin/structure/designer_entity",
 *   },
 *   field_ui_base_route = "designer_entity.settings"
 * )
 */
class DesignerEntity extends ContentEntityBase implements DesignerEntityInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Designer entity entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the Designer entity is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
