<?php

namespace Drupal\condrup\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Aircraft entity entities.
 */
class AircraftEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
