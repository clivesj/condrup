<?php

namespace Drupal\condrup\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Aircraft entity entity.
 *
 * @ingroup condrup
 *
 * @ContentEntityType(
 *   id = "aircraft_entity",
 *   label = @Translation("Aircraft entity"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\condrup\AircraftEntityListBuilder",
 *     "views_data" = "Drupal\condrup\Entity\AircraftEntityViewsData",
 *     "translation" = "Drupal\condrup\AircraftEntityTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\condrup\Form\AircraftEntityForm",
 *       "add" = "Drupal\condrup\Form\AircraftEntityForm",
 *       "edit" = "Drupal\condrup\Form\AircraftEntityForm",
 *       "delete" = "Drupal\condrup\Form\AircraftEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\condrup\AircraftEntityHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\condrup\AircraftEntityAccessControlHandler",
 *   },
 *   base_table = "aircraft_entity",
 *   data_table = "aircraft_entity_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer aircraft entity entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/aircraft_entity/{aircraft_entity}",
 *     "add-form" = "/admin/structure/aircraft_entity/add",
 *     "edit-form" = "/admin/structure/aircraft_entity/{aircraft_entity}/edit",
 *     "delete-form" = "/admin/structure/aircraft_entity/{aircraft_entity}/delete",
 *     "collection" = "/admin/structure/aircraft_entity",
 *   },
 *   field_ui_base_route = "aircraft_entity.settings"
 * )
 */
class AircraftEntity extends ContentEntityBase implements AircraftEntityInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Aircraft entity entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the Aircraft entity is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
