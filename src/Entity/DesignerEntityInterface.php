<?php

namespace Drupal\condrup\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Designer entity entities.
 *
 * @ingroup condrup
 */
interface DesignerEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Designer entity name.
   *
   * @return string
   *   Name of the Designer entity.
   */
  public function getName();

  /**
   * Sets the Designer entity name.
   *
   * @param string $name
   *   The Designer entity name.
   *
   * @return \Drupal\condrup\Entity\DesignerEntityInterface
   *   The called Designer entity entity.
   */
  public function setName($name);

  /**
   * Gets the Designer entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Designer entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Designer entity creation timestamp.
   *
   * @param int $timestamp
   *   The Designer entity creation timestamp.
   *
   * @return \Drupal\condrup\Entity\DesignerEntityInterface
   *   The called Designer entity entity.
   */
  public function setCreatedTime($timestamp);

}
