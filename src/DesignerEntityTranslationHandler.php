<?php

namespace Drupal\condrup;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for designer_entity.
 */
class DesignerEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
