<?php

namespace Drupal\condrup;

use Drupal\condrup\Services\Common;
use Drupal\user\Entity\User;

class ConscriboSync {

  public static function syncUser($user, &$context) {

    /**
     * @var User $user
     * @var Common $common
     */

    $common = \Drupal::service('condrup.common');
    // get the conscribo id
    $cid = $user->field_conscribo_id->value;
    // conscribo_data
    $message = 'Updating user...' . $user->getAccountName();
    $data = $common->cidConscriboUserFields($cid);
    // fixme: we should update email but should check interference
    // we don update e-mail
    unset($data['email']);
    foreach ($data as $row) {
      $user->set($row['label'], $row['value']);
    }

    // Save user
    if ($result = $user->save()) {
    };
    $context['message'] = $message ;
  }


  public static function syncUserFromConscribo($user, &$context) {
    /**
      $context['results'][] = $result;
     * @var User $user
     * @var Common $common
     */

    $common = \Drupal::service('condrup.common');
    $message = 'Updating user...' . $user->getAccountName();

    // get the conscribo id
    if ($cid = $user->field_conscribo_id->value) {
      // conscribo_data
      $data = $common->getFromConscribo($cid);
      // fixme: we should update email but should check interference
      debug($data);
      // we dont update e-mail
      unset($data['email']);
      foreach ($data as $row) {
        // $user->set($row['label'], $row['value']);
      }
      // Save user
//    if ($result = $user->save()) {
//      $context['results'][] = $result;
//    };
    }
    $context['message'] = $message ;
  }

}