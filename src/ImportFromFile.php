<?php

namespace Drupal\condrup;

use Drupal\condrup\Entity\AircraftEntity;
use Drupal\condrup\Entity\DesignerEntity;
use Drupal\condrup\Services\Common;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\user\Entity\User;

class ImportFromFile {

  /*
  * 0 = NUMMER
  */
  public static function importNAW($member, &$context) {
    /**
     * @var User $user
     * @var Common $common
     */
    $common = \Drupal::service('condrup.common');
    $nvav_old = $member[0];
    $message = 'Updating NAW ...';

    // get the user
    if ($uid = $common->idFromNVAV($nvav_old)) {
      if ($user = User::load($uid)) {
        // Update the fields from CSV
        // [0]=> "NUMMER" [3]=> "ADRES" [4]=> "ADRES2" [5]=> "POSTCODE" [6]=> "PLAATS" [7]=> "LAND" [9]=> "TELNR" [10]=> "MOBIEL" [11]=> "EMAIL"
        $user->set('field_adres_straat', $member[3]);
        $user->set('field_adres_straat_2', $member[4]);
        $user->set('field_adres_plaats', $member[6]);
        $user->set('field_adres_postcode', $member[5]);
        $user->set('field_adres_land', $member[7]);
        //$user->set('field_gegevens_delen', 'Volledig');
        if (!$user->isActive()) {
          $user->set('field_telephone', $member[9]);
          $user->set('field_mobile', $member[10]);
          if (!empty($member[11])) {
            $user->setEmail($member[11]);
          }
        }
        // Save user
        if ($result = $user->save()) {
          $context['results'][] = $result;
        };
        $context['message'] = $message;
      }
    }
  }

  /*
   * 0 = "relatie_nummer" 1 = "nvav_nummer" 2 = "voornaam" 3= "weergavenaam" 4 = "mail"
   */
  public static function importActive($member, &$context) {
    /**
     * @var User $user
     */
    $old_nvav_id_str = $member[1];
    $old_nvav_id_int = intval($member[1]);
    $conscribo_id = $member[0];
    $archive_name = $member[3];
    $mail = $member[4];
    $new_user_name = 'N' . $conscribo_id;

    $message = 'Importing member...' . $new_user_name ;

    // Check if user already exist
    $user = user_load_by_name($new_user_name);
    if (!$user) {
      // Create Drupal user
      $user = User::create();

      $user->setUsername($new_user_name);
      $user->setPassword(user_password());
      $user->setEmail($mail);
      $user->enforceIsNew();  // Set this to FALSE if you want to edit (re save) an existing user object

      // Optional settings
      $user->set('init', $mail);
      $user->set('field_conscribo_id', $conscribo_id);
      $user->set('field_archief_naam', $archive_name);
      $user->set('field_oude_nvav_id_int', $old_nvav_id_int);
      $user->set('field_oude_nvav_id_str', $old_nvav_id_str);
      $user->activate();

      // Save user
      if ($result = $user->save()) {
        $context['results'][] = $result;
      };
      $context['message'] = $message ;
    }
  }

  // 0 =relatienummer, 1 = naam, 2 =actieve_relatie
  public static function importArchived($member, &$context) {
    /**
     * @var User $user
     */
    $actief = $member[2] === 'J' ? TRUE : FALSE;
    $old_nvav_id_str = $member[0];
    $old_nvav_id_int = intval($member[0]);
    $new_user_name = 'XN' . $old_nvav_id_str;
    $archive_name = $member[1];
    $mail = $new_user_name . '@nvav.nl';

    if (!$actief) {
      $message = 'Importing member...' . $new_user_name . ' status: ' . $member[2];

      // Check if user already exist
      $user = user_load_by_name($new_user_name);
      if (!$user) {
        $user = User::create();

        // The Basics
        $user->setUsername($new_user_name);
        $user->setPassword(user_password());
        $user->setEmail($mail);

        // Optional settings
        $user->set('field_archief_naam', $archive_name);
        $user->set('field_oude_nvav_id_int', $old_nvav_id_int);
        $user->set('field_oude_nvav_id_str', $old_nvav_id_str);
        // do not activate

        // Save user
        if ($result = $user->save()) {
          $context['results'][] = $result;
        }
        $context['message'] = $message;
      }
      else {
        //user exists but let's update the archived name
        $user->set('field_archief_naam', $archive_name);
        if ($result = $user->save()) {
          $context['results'][] = $result;
          $context['message'] = 'updated';
        }
      }
    }
  }

  // Source [0] => "NUMMER" ; [1]=> "NAAM" ; [2]=> "_MEMOFIELD"
  public static function importDesigner($data, &$context) {
    /**
     * @var Common $common
     * @var DesignerEntity $designer
     */
    $common = \Drupal::service('condrup.common');
    $old_id = $data[0];
    $name = $data[1];
    $safe_name = $common->seo_friendly_url($name);

    $message = 'Importing: ' . $safe_name;
    $exists = current(\Drupal::entityTypeManager()
      ->getStorage('designer_entity')
      ->loadByProperties(['field_old_id' => $old_id]));
    if (!$exists) {
      $designer = DesignerEntity::create();
      $designer->setName($safe_name);
      $designer->set("field_old_id", $old_id);
      $designer->set("field_naam_ontwerper", $name);

      // Save
      if ($result = $designer->save()) {
        $context['results'][] = $result;
      }
      $context['results'][] = TRUE;
      $context['message'] = $message;
    }
  }

  // Source: 0 = NUMBER; 1 = OMSCHRIJ; 2 = SEATS; 3 = CATEGORIES; 4 =ONTW;
  // Destination:
  public static function importAircraftType($data, &$context) {
    /**
     * @var Common $common
     * @var AircraftEntity $aircraft
     */
    $common = \Drupal::service('condrup.common');
    $old_id = intval($data[0]); //
    $description = $data[1];  // string
    $safe_name = $common->seo_friendly_url($description);
    $designer_id = intval($data[4]);

    $message = 'Importing: ' . $safe_name;
    $exists = current(\Drupal::entityTypeManager()
      ->getStorage('aircraft_entity')
      ->loadByProperties(['field_old_id_int' => $old_id]));
    if (!$exists) {
      $aircraft = AircraftEntity::create();
      $aircraft->setName($safe_name);
      $aircraft->set("field_old_id_int", $old_id);
      $aircraft->set("field_designer", $designer_id);
      $aircraft->set('field_omschrijving', $description);

      // Save
     if ($result = $aircraft->save()) {
       $context['results'][] = $result;
     }
     $context['message'] = $message;
    }
  }

  public static function importOwnership($data, &$context) {
    /**
     * @var Common $common
     * @var NodeInterface $node
     */

    $rows = $data['rows'];
    $id = $data['id'];
    $node = Node::load($id);
    $nr = $node->field_project_nummer_oud->value;
    $message = 'Processing node ' . $node->id();


    foreach ($rows as $row) {
      $context['message']  = $message . ' | ' . $nr . ' | ' . $row['Eigendom'];
      if ($row['Projectnummer'] == $nr) {
        // set the values:
        $node->set('field_eigendom', $row['Eigendom']);
        $node->set('field_gecheckt', $row['Gecheckt']);
        $node->save();
        continue;
      }
    }

  }

  public static function importProject($data, &$context) {
    /**
     * @var Common $common
     * @var NodeInterface $node
     */
    // defaults
    $common = \Drupal::service('condrup.common');
    $drupal_uid = $common->idFromNvav($data['NUMMER'], 'drupal');
    $conscribo_id = $common->idFromNvav($data['NUMMER'], 'conscribo');
    $project_nr = $data['PROJNR'];
    $name = $common->displayNameFromUid($drupal_uid);

    $fields = [
      'type' => 'project',
      'field_project_nummer_oud' => intval($data['PROJNR']),
      'field_vliegtuig_type' => intval($data['TYPE']) -1,
      'field_inschrijf_datum' => $common->dateConvert($data['INSCHRD']),
      'field_ex_lid' => $data['EXLID'],
      'field_stadium' => $data['STADIUM'],
      'field_sleep' => $data['SLEEP'],
      'field_avionics' => [$data['AVIONICS']],
      'field_bouw_adviseur' => $common->idFromNvav($data['BEGELEID']) ? $common->idFromNvav($data['BEGELEID']) : null,
      'field_bouwer' => $common->idFromNvav($data['BOUWER']) ? $common->idFromNvav($data['BOUWER']) : null,
      'field_bouw_locatie' => $data['BOUWLOC'],
      'field_bvl' => $common->bvlConvert($data['BVLSTAT']),
      'field_datum_weging' => $common->dateConvert($data['DATWEG']),
      'field_eerste_vlucht' => $data['FIRSTFL'],
      'field_eigenaar' => $drupal_uid,
      'field_eindkeuring' => $common->dateConvert($data['KEURDAT']),
      'field_keurmeester' => $common->idFromNvav($data['KEURMEES']),
      'field_motor' => $data['MOTOR'],
      'field_opmerkingen' => [$data['MEMOFIELD']],
      'field_propellor' => $data['PROP'],
      'field_call_sign' => $data['CALSIGN'],
    ];

    $ids = false;
    // only create if node is not created earlier
      $ids = \Drupal::entityQuery('node')
        ->condition('type', 'project')
        ->condition('field_project_nummer_oud', $data['PROJNR'])
        ->execute();

    if (!$ids) {
      // create the node
      $title = $common->seo_friendly_url($project_nr . '-' . $name );
      $message = 'Importing: ' . $title;
      $node = Node::create($fields);
      $node->setTitle($title);
      if ($created = date_timestamp_get(date_create($common->dateConvert($data['INSCHRD'])))) {
        $node->setCreatedTime($created);
      }
      $node->setOwnerId($drupal_uid);
      $node->save();
      $context['results'][] = true;
      $context['message'] = $message;
    }
    else {
      // update the node
      $nid = reset($ids);
      $node = Node::load($nid);
      // update the memo field
      // $node->set('field_opmerkingen', $data['_MEMOFIELD']);
      // update the aircraft. Index was 1 too high
      $node->set('field_vliegtuig_type', $common->oldIdToAircraftEntity($data['TYPE']));
      $node->save();
    }
  }

  public static function finishedImport($success, $results, $operations) {
    // The 'success' parameter means no fatal PHP errors were detected. All
    // other error management should be handled using 'results'.
    if ($success) {
      $message = \Drupal::translation()->formatPlural(
        count($results),
        'One member processed.', '@count members processed.'
      );
    }
    else {
      $message = t('Finished with an error.');
    }
    \Drupal::messenger()->addMessage($message);
  }



}