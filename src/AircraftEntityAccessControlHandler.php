<?php

namespace Drupal\condrup;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Aircraft entity entity.
 *
 * @see \Drupal\condrup\Entity\AircraftEntity.
 */
class AircraftEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\condrup\Entity\AircraftEntityInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished aircraft entity entities');
        }


        return AccessResult::allowedIfHasPermission($account, 'view published aircraft entity entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit aircraft entity entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete aircraft entity entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add aircraft entity entities');
  }


}
