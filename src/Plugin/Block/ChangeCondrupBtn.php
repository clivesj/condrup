<?php
/**
*/

namespace Drupal\condrup\Plugin\Block;

use Drupal\Component\Serialization\Json;
use Drupal\condrup\ConscriboConnector\ConscriboConnector;
use Drupal\condrup\ConscriboConnector\Request\AddChangeTransactionRequest;
use Drupal\condrup\ConscriboConnector\Request\AuthenticateWithUserAndPassRequest;
use Drupal\condrup\ConscriboConnector\Request\ListAccountsRequest;
use Drupal\condrup\ConscriboConnector\Request\ListEntitiesRequest;
use Drupal\condrup\ConscriboConnector\Request\ListEntityTypesRequest;
use Drupal\condrup\ConscriboConnector\Request\ListFieldDefinitionsRequest;
use Drupal\condrup\ConscriboConnector\Request\ListRelationsRequest;
use Drupal\condrup\ConscriboConnector\Request\ListTransactionsRequest;
use Drupal\condrup\ConscriboConnector\Request\ListVatCodesRequest;
use Drupal\condrup\ConscriboConnector\Request\RemoveRelationRequest;
use Drupal\condrup\ConscriboConnector\Request\ReplaceRelationRequest;
use Drupal\condrup\ConscriboConnector\Response\ListEntityTypesResponse;
use Drupal\condrup\ConscriboConnector\Response\ListFieldDefinitionsResponse;
use Drupal\condrup\ConscriboConnector\Response\ListRelationsResponse;
use Drupal\condrup\ConscriboConnector\Response\ListTransactionsResponse;
use Drupal\condrup\ConscriboConnector\Response\ReplaceRelationResponse;
use Drupal\condrup\Services\Common;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;

/**
 * Block displaying Conscribo button.
 *
 * @Block(
 *   id = "conscribo_button_block",
 *   admin_label = @Translation("Conscribo button")
 * )
 */

class ChangeCondrupBtn extends BlockBase{

  /**
   * {@inheritdoc}
   */
  public function build() {
    /**
     * @var Common $common
     */
    $logged_in_uid = \Drupal::currentUser()->id();
    return [
      '#title' => $this->t('Edit personal data'),
      '#type' => 'link',
      '#url' => Url::fromRoute('condrup.edit.conscribo', array('cid' => $logged_in_uid)),
    ];
  }

  public function blockAccess(AccountInterface $account) {
    $display_uid = \Drupal::routeMatch()->getRawParameter('user');
   // debug($logged_in_uid, 'logged');
   // debug($display_uid, 'displayed');
    if ($account->id() == $display_uid) {
      return parent::blockAccess($account);
    }
    else {
      return AccessResult::forbidden();
    }
  }

}