<?php

/**
 * Created by PhpStorm.
 * User: joop
 * Date: 10/27/19
 * Time: 9:09 PM
 */

namespace Drupal\condrup\Plugin\Block;

use Drupal\Component\Serialization\Json;
use Drupal\condrup\ConscriboConnector\ConscriboConnector;
use Drupal\condrup\ConscriboConnector\Request\AddChangeTransactionRequest;
use Drupal\condrup\ConscriboConnector\Request\AuthenticateWithUserAndPassRequest;
use Drupal\condrup\ConscriboConnector\Request\ListAccountsRequest;
use Drupal\condrup\ConscriboConnector\Request\ListEntitiesRequest;
use Drupal\condrup\ConscriboConnector\Request\ListEntityTypesRequest;
use Drupal\condrup\ConscriboConnector\Request\ListFieldDefinitionsRequest;
use Drupal\condrup\ConscriboConnector\Request\ListRelationsRequest;
use Drupal\condrup\ConscriboConnector\Request\ListTransactionsRequest;
use Drupal\condrup\ConscriboConnector\Request\ListVatCodesRequest;
use Drupal\condrup\ConscriboConnector\Request\RemoveRelationRequest;
use Drupal\condrup\ConscriboConnector\Request\ReplaceRelationRequest;
use Drupal\condrup\ConscriboConnector\Response\ListEntityTypesResponse;
use Drupal\condrup\ConscriboConnector\Response\ListFieldDefinitionsResponse;
use Drupal\condrup\ConscriboConnector\Response\ListRelationsResponse;
use Drupal\condrup\ConscriboConnector\Response\ListTransactionsResponse;
use Drupal\condrup\ConscriboConnector\Response\ReplaceRelationResponse;
use Drupal\condrup\Services\Common;
use Drupal\Core\Block\BlockBase;


/**
 * Block of Cat Facts... you can't make this stuff up.
 *
 * @Block(
 *   id = "cat_facts_block",
 *   admin_label = @Translation("Cat Facts")
 * )
 */
class CatFacts extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    /**
     * @var Common $common
     */
 //   /** @var \GuzzleHttp\Client $client */
    // watchdog_exception('condrup', 'started');
//
//    $base_uri = Common::END_POINT;
//
//    $client = \Drupal::service('http_client_factory')->fromOptions([
//      'base_uri' => 'https://secure.conscribo.nl/nvav',
//    ]);
//
//    try {
//      $response = $client->get('/json.api', [],
//          json_encode(
//             [
//              'method' => 'authenticateWithUserAndPass',
//              'userName' => 'webkoppeling',
//              'passPhrase' => Common::API_USER_PASS,
//          ]
//      ));
//        $data = $response->getStatusCode() . ' | ' . print_r($response->getHeaders());
//    }
//    catch (RequestException $e) {
//      watchdog_exception('condrup', $e->get);
//    }
//
//
//    $cat_facts = Json::decode($response->getBody());
//    drupal_set_message('hier' . $data );
//    $items = [];
//
//    if($cat_facts) {
//      foreach ($cat_facts as $cat_fact) {
//        $items[] = $cat_fact['text'];
//      }
//    }

    //  $_SESSION['joop'] = '123456';

    //  $api = \Drupal::service('condrup.api');
    $common = \Drupal::service('condrup.common');
    $connector = $common->createConscriboConnector();
      // RELATION RELATED REQUESTS //
      $this->runRelationExample($connector);

      // FINANCIAL RELATED REQUESTS //

     // $this->runFinancialExamples($connector);
  }


    function runRelationExample(ConscriboConnector $connector) {
      /**
       * @var ListEntityTypesResponse $entityTypes
       */
      $entityTypes = $connector->execute(ListEntityTypesRequest::create());

      echo 'Existing entityTypes: ' . implode(', ', $entityTypes->getEntityTypeNames()) . "\n\n";

      // LIST FIELDDEFINITIONS
      /**
       * @var ListFieldDefinitionsResponse $fieldDefinitions
       */
      $fieldDefinitions = $connector->execute(ListFieldDefinitionsRequest::create()->setEntityType('persoon'));

      echo 'Read ' . $fieldDefinitions->count() . ' fieldDefinitions' . "\n\n";

//      // LIST FIELDDEFINITIONS
//      /**
//       * @var \ConscriboConnector\Response\ListFieldDefinitionsResponse $fieldDefinitions
//       */
//      $fieldDefinitions = $connector->execute(\ConscriboConnector\Request\ListFieldDefinitionsRequest::create()
//        ->setEntityType('persoon'));
//
//      echo 'Read ' . $fieldDefinitions->count() . ' fieldDefinitions' . "\n\n";

      // LIST RELATIONS
      /**
       * @var ListRelationsRequest   $relationsRequest
       * @var ListRelationsResponse $relations
       */
      $relationsRequest = ListRelationsRequest::create();
      $relationsRequest->setFieldNames($fieldDefinitions->getFieldNames())
        ->addTextFilter('naam', '~', 'e')// Find everyone with an e in the surname
        ->setLimit(5);

      $relations = $connector->execute($relationsRequest);
      echo 'Retrieved ' . $relations->count() . ' of ' . $relations->getTotalResultCount() . ' found relations' . "\n";

      foreach($relations->getRelations() as $relation) {
        echo 'Received: ' . $relation['selector'] . "\n";
      }

      // NEW RELATION

      /**
       * @var ReplaceRelationRequest $newRelationRequest
       */
      $newRelationRequest = ReplaceRelationRequest::create();
      $newRelationRequest->setModeNewRelation();

      foreach($fieldDefinitions->getFieldNames() as $fieldName) {
        if($fieldDefinitions->getFieldType($fieldName) == 'string'
          && !$fieldDefinitions->getFieldIsReadOnly($fieldName)
        ) {
          $newRelationRequest->setValue($fieldName, str_rot13($fieldName));
        }
      }

      $newRelationRequest->setValue('naam', 'Piet');
      /**
       * @var ReplaceRelationRequest $newRelation
       */
      $newRelation = $connector->execute($newRelationRequest);
      echo 'Added relation with nr ' . $newRelation->getRelationNr() . "\n";

      // EXAMPLE UPDATE RELATION, MULTIPLEREQUESTMODE:

      $res = array();

      foreach($relations->getRelations() as $relation) {
        /**
         * @var ReplaceRelationRequest $updateRequest
         */
        $updateRequest = ReplaceRelationRequest::create();

        echo 'Changing ' . $relation['naam'] . ' to ' . str_replace('e', 'ee', $relation['naam']) . "\n";
        $updateRequest->setModeUpdateRelation($relation['code'])
          ->setValue('naam', str_replace('e', 'ee', $relation['naam']));    // update naam, change all 'e' to dubble 'ee'
        $res[$relation['code']] = $connector->schedule($updateRequest);
      }

      $connector->commit();

      foreach($res as $relationNr => $request) {
        /**
         * @var ReplaceRelationRequest   $request
         * @var ReplaceRelationResponse $updatedRelation
         */
        $updatedRelation = $request->getResponse();
        $relation = $relations->getRelationWithNr($relationNr);
        echo $relation['selector'] . ' changed' . "\n";
      }

      // REMOVE RELATION

      $removeRelationRequest = RemoveRelationRequest::create();
      /**
       * @var RemoveRelationRequest $removeRelationRequest
       */
      $removeRelationRequest->setRelationNr($newRelation->getRelationNr());
      $response = $connector->execute($removeRelationRequest);

      if($response->getIsSuccessFull()) {
        echo 'Relation ' . $newRelation->getRelationNr() . ' removed' . "\n";
      }

    }


    function runFinancialExamples(ConscriboConnector $connector) {

      $newTransaction = AddChangeTransactionRequest::create();
      /**
       * @var AddChangeTransactionRequest $newTransaction
       */
      $newTransaction->setModeNew()
        ->setDate(date('Y-m-d'))
        ->setDescription('One transaction')
        ->addTransactionRow('1000', 100, 'F-1')
        ->addTransactionRow('4000', -50, 'F-1')
        ->addTransactionRow('8000', -50, 'F-1');

      $resp = $connector->execute($newTransaction);

      if($resp->getIsSuccessFull()) {
        echo 'Transaction with Id ' . $resp->getTransactionId() . " added.\n";
      }


      // LIST TRANSACTIONS:
      $transactions = ListTransactionsRequest::create();
      /**
       * @var ListTransactionsRequest $transactions
       */

      $transactions->addDateStartFilter(date('Y-m-d'));
      $transactions->addAccountNrFilter(4000);
      $transactions->addReferenceFilter('F-1');

      $result = $connector->execute($transactions);
      /**
       * @var ListTransactionsResponse $result ;
       */
      if($result->getIsSuccessFull()) {
        echo 'Received ' . $result->count() . ' of ' . $result->getTotalResultCount() . ' transactions' . "\n";
        var_export($result->getTransactions());
      }

      $accountsRequest = ListAccountsRequest::create();
      /**
       * @var ListAccountsRequest $accountsRequest ;
       */

      $accounts = $connector->execute($accountsRequest);
      echo 'Received ' . $accounts->count() . ' Accounts ' . "\n";


      $vatCodesRequest = ListVatCodesRequest::create();
      /**
       * @var ListVatCodesRequest $vatCodesRequest
       */
      $vatCodes = $connector->execute($vatCodesRequest);

      echo 'Received ' . $vatCodes->count() . ' Vatcodes ' . "\n";

    return [
      //'#theme' => 'item_list',
    // '#markup' => 'SESSION ID '  . $api->getSessionId(),
      '#markup' => 'Sxxx',
    ];
  }


}