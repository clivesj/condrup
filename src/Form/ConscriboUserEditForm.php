<?php
/**
 * Created by PhpStorm.
 * User: joop
 * Date: 1/9/20
 * Time: 12:54 PM
 */

// fixme: not used anymore

namespace Drupal\Condrup\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;

class ConscriboUserEditForm extends FormBase {


  /**
   * @var \Drupal\condrup\Services\Common
   */
  protected $common;

  protected $dataFields;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'conscribo_edir_user_form';
  }

  /**
   * {@inheritdoc}
   * @var array $list
   */
  public function buildForm(array $form, FormStateInterface $form_state, $nid = null, $relative_fid = null, $fids = null, $ajax = null) {
    $this->common = \Drupal::service('condrup.common');

    $this->dataFields = [
      'telefoon' => 'Phone number',
      'mobiel' => 'Mobile phone number',
    ];

    // If this form is to be presented in a slide-down window we
    // will set the attributes and at a close-window link
    if($ajax) {
      $form['#attributes'] = [
        'class' => [
          'form-in-slide-down'
        ],
      ];
      $form['close-window'] = $this->common->closeButtonMarkup();
    }

    $form['#tree'] = true;

    foreach ($this->dataFields as $name => $label) {
      $form['return_values'][$name] = [
        '#type' => 'textfield',
        '#title' => $this->t($label),
        '#size' => 40,
        '#description' => $this->t(''),
        '#required' => TRUE,
      ];
    }

    $form['save'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#name' => 'create',
    ];
    $form['cancel'] = [
      '#type' => 'submit',
      '#value' => $this->t('Cancel'),
      '#name' => 'create',
    ];

    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
  //  $form['#attached']['library'][] = 'filebrowser/filebrowser-styles';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
//  public function validateForm(array &$form, FormStateInterface $form_state) {
//      return
//  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue('return_values');
    $result = false;
    $data = [];
    $uid = \Drupal::currentUser()->id();
    $account = User::load($uid);
    $cid = $account->field_conscribo_id->value;
    if ($cid) {
      foreach ($values as $field_name => $value) {
        $data[$field_name] = $value;
      }
      $result = $this->common->editUser($cid, $data);
    }
    if ($result) {
      \Drupal::messenger()->addMessage($this->t('Your data is updated successfully'));
    }
    else {
      \Drupal::messenger()->addError($this->t('Can not update, please contact the administrator'));
    }
    Cache::invalidateTags(['user:' . $uid]);
    $form_state->setRedirect('user.page');
  }

}