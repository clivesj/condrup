<?php

namespace Drupal\condrup\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Aircraft entity entities.
 *
 * @ingroup condrup
 */
class AircraftEntityDeleteForm extends ContentEntityDeleteForm {


}
