<?php

namespace Drupal\condrup\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Designer entity entities.
 *
 * @ingroup condrup
 */
class DesignerEntityDeleteForm extends ContentEntityDeleteForm {


}
