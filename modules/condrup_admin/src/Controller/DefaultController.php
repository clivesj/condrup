<?php

namespace Drupal\condrup_admin\Controller;

use Drupal\Core\Controller\ControllerBase;

class DefaultController extends ControllerBase{

  // todo: disabled move to sub module

//  /**
//   * Callback for condrup/import/users
//   * csv sep is ';'
//   *
//   *
//   * @return array
//   */
//  public function importArchivedMembers() {
//
//    $filename = 'public://nvav_relaties_selectie.csv';
//    if (file_exists($filename)) {
//      // The nested array to hold all the arrays
//      $results = [];
//      // Open the file for reading
//      if (($h = fopen("{$filename}", "r")) !== FALSE) {
//        // Each line in the file is converted into an individual array that we call $data
//        // The items of the array are comma separated
//        while (($data = fgetcsv($h, 1000, ",")) !== FALSE) {
//          //Each individual array is being pushed into the nested array
//          $results[] = $data;
//        }
//        // Close the file
//        fclose($h);
//
//        // Display results
//        $batch = array(
//          'title' => t('Importing members...'),
//          'operations' => [],
//          'init_message'     => $this->t('Start import'),
//          'progress_message' => $this->t('Processed @current out of @total.'),
//          'error_message'    => $this->t('An error occurred during processing'),
//          'finished' => '\Drupal\condrup\ImportMember::finishedImport',
//        );
//        // delete the first row (labels)
//        unset($results[0]);
//        foreach($results as $result) {
//          $batch['operations'][] = ['Drupal\condrup\ImportFromFile::importArchived', [$result]];
//        }
//        batch_set($batch);
//        return batch_process('admin/people');
//      }
//    }
//    else {
//      \Drupal::messenger()->addError($this->t('File doe not exist'));
//
//      return [
//        '#markup' => 'result'
//      ];
//    }
//  }

  // todo: disabled move to sub module

//  /**
//   * Callback for condrup/import/users
//   * csv sep is ';'
//   *
//   *
//   * @return array
//   */
//  public function importActiveMembers() {
//
//    $filename = 'public://con_rel_export.csv';
//    if (file_exists($filename)) {
//      // The nested array to hold all the arrays
//      $results = [];
//      // Open the file for reading
//      if (($h = fopen("{$filename}", "r")) !== FALSE) {
//        // Each line in the file is converted into an individual array that we call $data
//        // The items of the array are comma separated
//        while (($data = fgetcsv($h, 1000, ";")) !== FALSE) {
//          //Each individual array is being pushed into the nested array
//          $results[] = $data;
//        }
//        // Close the file
//        fclose($h);
//
//        // Display results
//        $batch = array(
//          'title' => t('Importing members...'),
//          'operations' => [],
//          'init_message'     => $this->t('Start import'),
//          'progress_message' => $this->t('Processed @current out of @total.'),
//          'error_message'    => $this->t('An error occurred during processing'),
//          'finished' => '\Drupal\condrup\ImportFromFile::finishedImport',
//        );
//
//        // delete the first row (labels)
//        var_dump($results[0]);
//        unset($results[0]);
//        foreach($results as $key => $result) {
//          $batch['operations'][] = ['Drupal\condrup\ImportFromFile::importActive',[$result]];
//        }
//        batch_set($batch);
//        return batch_process('admin/people');
//      }
//    }
//    else {
//      \Drupal::messenger()->addError($this->t('File does not exist'));
//      return [
//        '#markup' => 'result'
//      ];
//    }
//  }

  // todo: disabled move to sub module

//  /**
//   * Callback for condrup/import/designer
//   * csv sep is ';'
//   * [
//   * 0 = NUMMER 1 = OMSCHRIJ 2 = SEATS 3 = CATEGORI; 4 = ONTW ;STDLEEG;STDMTOW;SPW;LENGTE;HOOGTE;OPP;AANTTANK;ENDURANC;TANKINH;ONDERSTE;INTR;VNE;VNO;VFE;VS;VSO;VY;RC;START;START15;LANDING;LANDIN15;_MEMOFIELD
//   *
//   *
//   * @return array
//   */
//  public function importAircraft() {
//
//    $filename = 'public://types.export.csv';
//    if (file_exists($filename)) {
//      // The nested array to hold all the arrays
//      $results = [];
//      // Open the file for reading
//      if (($h = fopen("{$filename}", "r")) !== FALSE) {
//        // Each line in the file is converted into an individual array that we call $data
//
//        while (($data = fgetcsv($h, 1000, ";")) !== FALSE) {
//          //Each individual array is being pushed into the nested array
//          $results[] = $data;
//        }
//        // Close the file
//        fclose($h);
//
//        debug($results);
//
//        // Display results
//        $batch = array(
//          'title' => t('Importing aircraft type...'),
//          'operations' => [],
//          'init_message'     => $this->t('Start import'),
//          'progress_message' => $this->t('Processed @current out of @total.'),
//          'error_message'    => $this->t('An error occurred during processing'),
//          'finished' => '\Drupal\condrup\Import::finishedImport',
//        );
//        // delete the first row (labels)
//        unset($results[0]);
//        foreach($results as $result) {
//          $batch['operations'][] = ['Drupal\condrup\ImportFromFile::importAircraftType',[$result]];
//        }
//    //    batch_set($batch);
//      //  return batch_process('admin/structure/aircraft_entity');
//      }
//      return [
//        '#markup' => 'result'
//      ];
//    }
//    else {
//      \Drupal::messenger()->addError($this->t('File doe not exist'));
//
//      return [
//        '#markup' => 'result'
//      ];
//    }
//  }
//
//  /**
//   * Callback for condrup_admin/import/designer2
//   * csv sep is ';'
//   *
//   *
//   * @return array
//   */
//  public function importDesigner() {
//
//    $filename = 'public://ontwerper.export.csv';
//    if (file_exists($filename)) {
//      // The nested array to hold all the arrays
//      $results = [];
//      // Open the file for reading
//      if (($h = fopen("{$filename}", "r")) !== FALSE) {
//        // Each line in the file is converted into an individual array that we call $data
//
//        while (($data = fgetcsv($h, 1000, ";")) !== FALSE) {
//          //Each individual array is being pushed into the nested array
//          $results[] = $data;
//        }
//        // Close the file
//        fclose($h);
//
//        // Display results
//        $batch = array(
//          'title' => t('Importing designer...'),
//          'operations' => [],
//          'init_message'     => $this->t('Start import'),
//          'progress_message' => $this->t('Processed @current out of @total.'),
//          'error_message'    => $this->t('An error occurred during processing'),
//          'finished' => '\Drupal\condrup\Import::finishedImport',
//        );
//
//        // delete the first row (labels)
//        unset($results[0]);
//        foreach($results as $result) {
//          $batch['operations'][] = ['Drupal\condrup\ImportFromFile::importDesigner',[$result]];
//        }
//        batch_set($batch);
//        return batch_process('admin/structure/designer_entity');
//      }
//      return [
//        '#markup' => 'result'
//      ];
//    }
//    else {
//      \Drupal::messenger()->addError($this->t('File doe not exist'));
//
//      return [
//        '#markup' => 'result'
//      ];
//    }
//  }

// todo: disabled move to sub module

//  /**
//   * Callback for condrup/import/ownership
//   * csv sep is ';'
//   *
//   *
//   * @return array
//   */
//  public function importOwnership() {
//    /**
//     * @var \Drupal\condrup\Services\Common $common
//     */
//    $common = \Drupal::service('condrup.common');
//    $filename = 'public://nvav_projecten.csv';
//    if (file_exists($filename)) {
//      // The nested array to hold all the arrays
//      $results = [];
//      // Open the file for reading
//      if (($h = fopen("{$filename}", "r")) !== FALSE) {
//        // Each line in the file is converted into an individual array that we call $data
//
//        while (($data = fgetcsv($h, 1000, ",")) !== FALSE) {
//          //Each individual array is being pushed into the nested array
//          $results[] = $data;
//        }
//        // Close the file
//        fclose($h);
//
//        $rows = $common->arrayReplaceKeys($results);
//
//        // remove row with labels
//        unset($rows[0]);
//      }
//      else {
//        \Drupal::messenger()->addError($this->t('File doe not exist'));
//      }
//      // setup the batch
//      $batch = array(
//        'title' => t('Importing ownership...'),
//        'operations' => [],
//        'init_message' => $this->t('Start import'),
//        'progress_message' => $this->t('Processed @current out of @total.'),
//        'error_message' => $this->t('An error occurred during processing'),
//        //'finished' => '\Drupal\condrup\Import::finishedImportOwnership',
//      );
//
//      // get all the projects
//      $ids = \Drupal::entityQuery('node')
//        ->condition('type', 'project')
//        ->execute();
//
//
//      foreach ($ids as $id) {
//        $data = [
//          'rows' => $rows,
//          'id' => $id,
//        ];
//        $batch['operations'][] = [
//          'Drupal\condrup\ImportFromFile::importOwnership',
//          [$data]
//        ];
//        // echo('Source: ' . $row['INSCHRD'] . "<br>Dest : " . $common->dateConvert($row['INSCHRD']) . "<br>");
//      }
//      batch_set($batch);
//      return batch_process('admin/content');
//    }
//  }

  /**
   * Callback for condrup/import/designer
   * csv sep is ';'
   *
   *
   * @return array
   */
  public function importProject() {
    /**
     * @var \Drupal\condrup\Services\Common $common
     */
    $common = \Drupal::service('condrup.common');
    $filename = 'public://projecten.export.csv';
    if (file_exists($filename)) {
      // The nested array to hold all the arrays
      $results = [];
      // Open the file for reading
      if (($h = fopen("{$filename}", "r")) !== FALSE) {
        // Each line in the file is converted into an individual array that we call $data

        while (($data = fgetcsv($h, 1000, ";")) !== FALSE) {
          //Each individual array is being pushed into the nested array
          $results[] = $data;
        }
        // Close the file
        fclose($h);

        $rows = $common->arrayReplaceKeys($results);

        // remove row with labels
        unset($rows[0]);
        $batch = array(
          'title' => t('Importing designer...'),
          'operations' => [],
          'init_message'     => $this->t('Start import'),
          'progress_message' => $this->t('Processed @current out of @total.'),
          'error_message'    => $this->t('An error occurred during processing'),
          'finished' => '\Drupal\condrup\Import::finishedImport',
        );

        foreach($rows as $row) {
          $batch['operations'][] = ['Drupal\condrup\ImportFromFile::importProject', [$row]];
         // echo('Source: ' . $row['INSCHRD'] . "<br>Dest : " . $common->dateConvert($row['INSCHRD']) . "<br>");
        }
        batch_set($batch);
        return batch_process('admin/content');
      }
      return [
        '#markup' => 'result'
      ];
    }
    else {
      \Drupal::messenger()->addError($this->t('File doe not exist'));

      return [
        '#markup' => 'result'
      ];
    }
  }




//  /**
//   * Callback for
//   * route: filebrowser.page_download
//   * path: filebrowser/download/{fid}
//   * @param int $fid Id of the file selected in the download link
//   * @return \Symfony\Component\HttpFoundation\RedirectResponse
//   */
//  public function pageDownload($fid) {
//    /* @var NodeInterface $node **/
//    $node_content = $this->common->nodeContentLoad($fid);
//    // If $fid doesn't point to a valid file, $node_content is FALSE.
//    if (!$node_content) {
//      throw new NotFoundHttpException();
//    }
//    $file_data = unserialize($node_content['file_data']);
//    $filebrowser = new Filebrowser($node_content['nid']);
//
//    // Download method is 'public' and the uri is public://
//    // we will send the browser to the file location.
//    // todo:
//    // RedirectResponse needs a relative path so we will convert the full url into a relative path
//    // This is done here, but should be moved to a better place in Common
//    $file_path = file_url_transform_relative($file_data->url);
//    if ($filebrowser->downloadManager == 'public' && \Drupal::service('file_system')->uriScheme($file_data->uri) == 'public') {
//      $response = new RedirectResponse($file_path);
//      return $response;
//    }
//    // we will stream the file
//    else {
//      // load the node containing the file so we can check
//      // for the access rights
//      // User needs "view" permission on the node to download the file
//      $node = Node::load($node_content['nid']);
//      if (isset($node) && $node->access('view')) {
//        // Stream the file
//        $file = $file_data->uri;
//        // in case you need the container
//        //$container = $this->container;
//        $response = new StreamedResponse(function () use ($file) {
//          $handle = fopen($file, 'r') or exit("Cannot open file $file");
//          while (!feof($handle)) {
//            $buffer = fread($handle, 1024);
//            echo $buffer;
//            flush();
//          }
//          fclose($handle);
//        });
//        $response->headers->set('Content-Type', $file_data->mimetype);
//        $content_disposition = $filebrowser->forceDownload ? 'attachment' : 'inline';
//        $response->headers->set('Content-Disposition', $content_disposition . '; filename="' . $file_data->filename . '";');
//        return $response;
//
//      }
//      elseif (isset($node)) {
//        throw new AccessDeniedHttpException();
//      }
//      else {
//        throw new NotFoundHttpException();
//      }
//    }
//  }
//
//  /**
//   * @param int $nid
//   * @param int $query_fid In case of a sub folder, the fid of the sub folder
//   * @param string $op - The operation called by the submit button ('upload', 'delete')
//   * @param string $method - Defines if Ajax should be used
//   * @param string|null $fids A string containing the field id's of the files
//   * to be processed.
//   *
//   * @return \Drupal\Core\Ajax\AjaxResponse|\Drupal\Core\Render\HtmlResponse
//   */
//  public function actionFormSubmitAction($nid, $query_fid, $op, $method, $fids = NULL) {
//    // $op == archive does not use a form
//    if ($op == 'archive') {
//      return $this->actionArchive($fids);
//    }
//
//    // continue for buttons needing a form
//    // Determine the requested form name
//    $op = ucfirst($op);
//    $form_name = 'Drupal\filebrowser\Form\\' . $op . 'Form';
//    //debug($form_name);
//    $form = \Drupal::formBuilder()->getForm($form_name, $nid, $query_fid, $fids, $method == 'ajax');
//
//    // If JS enabled
//    if ($method == 'ajax' && $op <> 'Archive') {
//
//      // Create an AjaxResponse.
//      $response = new AjaxResponse();
//      // Remove old error in case they exist.
//      $response->addCommand(new RemoveCommand('#filebrowser-form-action-error'));
//      // Remove slide-downs if they exist.
//      $response->addCommand(new RemoveCommand('.form-in-slide-down'));
//      // Insert event details after event.
//      $response->addCommand(new AfterCommand('#form-action-actions-wrapper', $form));
//      return $response;
//    }
//    else {
//      return $form;
//    }
//  }
//
//  public function inlineDescriptionForm($nid, $query_fid, $fids) {
//    return \Drupal::formBuilder()->getForm('Drupal\filebrowser\Form\InlineDescriptionForm', $nid, $query_fid, $fids);
//  }
//
//  /**
//   * @function
//   * zip file will be written to the temp directory on the local filesystem.
//   * @param $fids
//   * @return BinaryFileResponse|bool The binary response object or false if method cannot create archive
//   */
//  public function actionArchive($fids) {
//    $fid_array = explode(',', $fids);
//    $itemsToArchive = null;
//    $itemsToArchive = $this->common->nodeContentLoadMultiple($fid_array);
//    $file_name = \Drupal::service('file_system')->realPath('public://' . uniqid('archive') . '.zip');
//    $archive = new \ZipArchive();
//    $created = $archive->open($file_name, \ZipArchive::CREATE);
//
//    if ($created === TRUE) {
//      foreach ($itemsToArchive as $item) {
//        $file_data = unserialize($item['file_data']);
//        if ($file_data->type == 'file') {
//          $archive->addFile(\Drupal::service('file_system')->realpath($file_data->uri), $file_data->filename);
//        }
//        if ($file_data->type == 'dir') {
//          $dirPath = \Drupal::service('file_system')->realpath($file_data->uri);
//          // Iterate through the directory, adding each file within
//          $iterator = new \RecursiveDirectoryIterator($dirPath);
//          // Skip files that begin with a dot
//          $iterator->setFlags(\RecursiveDirectoryIterator::SKIP_DOTS);
//          $dirFiles = new \RecursiveIteratorIterator($iterator, \RecursiveIteratorIterator::SELF_FIRST);
//
//          foreach ($dirFiles as $dirFile) {
//            if (is_dir($dirFile)) {
//              $archive->addEmptyDir(str_replace(dirname($dirPath) . '/', '', $dirFile . ''));
//            } else if (is_file($dirFile)) {
//              $archive->addFromString(str_replace(dirname($dirPath) . '/', '', $dirFile), file_get_contents($dirFile));
//            }
//          }
//        }
//      }
//      $name = $archive->filename;
//      $archive->close();
//      // serve the file
//      $response = new BinaryFileResponse($name);
//      $response->deleteFileAfterSend(true);
//      $response->trustXSendfileTypeHeader();
//      $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
//      $response->prepare(Request::createFromGlobals());
//      return $response;
//    }
//    else {
//      \Drupal::logger('filebrowser')->error($this->t('Can not create archive: @error', ['@error' => $created]));
//      \Drupal::messenger()->addError($this->t('Can not create archive'));
//      return false;
//    }
//  }
//
//  public function noItemsError() {
//    $error = $this->t('You didn\'t select any item');
//
//    // Create an AjaxResponse.
//    $response = new AjaxResponse();
//    // Remove old events
//    $response->addCommand(new RemoveCommand('#filebrowser-form-action-error'));
//    $response->addCommand(new RemoveCommand('.form-in-slide-down'));
//    // Insert event details after event.
//    // $response->addCommand(new AfterCommand('#form-action-actions-wrapper', $html));
//    $response->addCommand(new AlertCommand($error));
//    return $response;
//  }

  /**
   * Callback for condrup/import/naw
   * csv sep is ';'
   *
   * Due to error I have to import NAW of members
   *
   * @return array
   */

  public function importNAW() {
    $filename = 'public://nvav.csv';
    if (file_exists($filename)) {
      // The nested array to hold all the arrays
      $results = [];
      // Open the file for reading
      if (($h = fopen("{$filename}", "r")) !== FALSE) {
        // Each line in the file is converted into an individual array that we call $data
        // The items of the array are comma separated
        while (($data = fgetcsv($h, 1000, ",")) !== FALSE) {
          //Each individual array is being pushed into the nested array
          $results[] = $data;
        }
        // Close the file
        fclose($h);

        // Display results
        $batch = array(
          'title' => t('Updating NAW...'),
          'operations' => [],
          'init_message'     => $this->t('Start import'),
          'progress_message' => $this->t('Processed @current out of @total.'),
          'error_message'    => $this->t('An error occurred during processing'),
          'finished' => '\Drupal\condrup\ImportFromFile::finishedImport',
        );

        // delete the first row (labels)
        unset($results[0]);
        foreach($results as $key => $result) {
          $batch['operations'][] = ['Drupal\condrup\ImportFromFile::importNAW',[$result]];
        }
        batch_set($batch);
        return batch_process('admin/people');
      }
      return [
        '#markup' => 'result'
      ];
    }
    else {
      \Drupal::messenger()->addError($this->t('File does not exist'));
      return [
        '#markup' => 'result'
      ];
    }
  }

}