<?php

/**
 * @file
 * Contains designer_entity.page.inc.
 *
 * Page callback for Designer entity entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Designer entity templates.
 *
 * Default template: designer_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_designer_entity(array &$variables) {
  // Fetch DesignerEntity Entity Object.
  $designer_entity = $variables['elements']['#designer_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
