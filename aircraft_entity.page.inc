<?php

/**
 * @file
 * Contains aircraft_entity.page.inc.
 *
 * Page callback for Aircraft entity entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Aircraft entity templates.
 *
 * Default template: aircraft_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_aircraft_entity(array &$variables) {
  // Fetch AircraftEntity Entity Object.
  $aircraft_entity = $variables['elements']['#aircraft_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
